export interface IProfileModel {
    loadingUser: boolean;
    errorUser:   string;
    user:        IUserModel;
    wishList:    any;
    reviews:     any;
}

export interface IUserModel {
    city: string;
    created: string;
    email: string;
    firstname: string;
    id: string;
    lastname: string;
    phone: string;
    address: {
        index: string;
        street: string;
        house: string;
        apartment: string;
        country: string;
    };
}

export interface IUserModelResponse {
    data: IUserModel;
}