import {IAnnouncement} from "../models/announcements.model";
import {IUserModel} from "../models/user.model";

export interface IMessageModel {
    created: string,
    messageId?: string,
    text: string,
    chatId?: string,
    userId?: string
}

export interface IUserChatModel {
    announcement: IAnnouncement,
    chatId: string,
    messages: IMessageModel[],
    recipient: IUserModel,
    user: IUserModel
}
