import { IUserModel } from "./user.model";

export interface IBrand {
    name: string;
    id:   string;
}

export interface IPerfume {
    name: string;
    id: string;
    brand: IBrand;
}

export interface IFilter {
    price: number[];
    name: IPerfume | "",
    brand: IBrand | "",
    city: string;
    isNew: string;
    bottleType: string;
    country: string;
}

export interface IAnnouncements {
    loading: boolean;
    error: string;
    announcementsItems: [] | Array<IAnnouncement>;
    count: number;
    announcementById: IAnnouncement;
    filter: IFilter;
    showFilter: boolean;
}

export interface IAnnouncement {
    isNew:  any;
    images: any;
    name:   string;
    description: string;
    price: number;
    brand: string;
    is_bottle: string;
    is_new: string;
    id: string;
    volume: string;
    user: IUserModel;
    created: Date;
}

export interface IAnnouncementsResponse {
    data: {
        data: Array<IAnnouncement>;
        count: number;
    }
}

export interface IAnnouncementResponse {
    data: {
        data: IAnnouncement;
    }
}

export interface IState {
    name: string;
}

export interface IFormat {
    name: string;
}

