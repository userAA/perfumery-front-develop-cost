export interface IAuthModel {
    loading: boolean | null;
    isAuth: boolean;
    stepModal: string;
    error: string;    
    modalIsOpen: boolean;
    userId: string;
    username: string;
    lastname: string;
    email:    string;
    registError: string;
    confirmError: string;
}

export type TStepModal = 'auth'|'email'|'regist'|'code';

export interface ITokenResponse {
    data: {
        bearerToken: string;
        refreshToken: string;
    }
}

export interface IRegistResponse {
    data: {
        data: {
            userId: string;
        }
    }
}

