"use client";

import Header from "@/components/Header/Header";
import {Roboto} from "next/font/google";
import { ReduxProvider } from "@/store/Provider";

import Footer from "@/components/Footer/Footer";

const adventPro = Roboto({
  weight: ["100", "300", "400", "500", "700", "900"],
  subsets: ["latin"],
  variable: "--adventPro"
})

const RootLayout = ({children}: {children: React.ReactNode;}) => 
{
  return (
    <html lang="ru">
      <body className={adventPro.variable}>
        <ReduxProvider>
          <Header/>
          {children}
          <Footer/>
        </ReduxProvider>
      </body>
    </html>
  );
}

export default RootLayout;