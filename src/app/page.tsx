"use client"
import Filter from "../components/ComponentsCatalog/Filter/Filter";
import Wrapper from "@/components/UI/Wrapper";
import { getAllAnnouncement } from "@/store/actions/announcementsActions";
import {useAppDispatch, useAppSelector} from "@/store/hooks/redux";
import { useEffect, useState } from "react";
import Baner from "@/components/Baner/Baner";
import Catalog from "@/components/ComponentsCatalog/Catalog/Catalog";

const Home = () => {
  const dispatch = useAppDispatch();
  const {announcementsItems, count, filter} = useAppSelector( (state) => state.announcements);

  const [limit, setLimit] = useState(20);

  useEffect(() => {
    dispatch(getAllAnnouncement(limit, 0, filter));
  }, [limit, filter]);

  return (
    <main>
      <Baner/>
      <Filter count={count}></Filter>
      <Wrapper>
        <Catalog
          type={"main"}
          items={announcementsItems}
          count={count}
          limit={limit}
          setLimit={setLimit}
        />
      </Wrapper>
    </main>
  );
}

export default Home;