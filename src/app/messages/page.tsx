import GeneralView from "@/components/ComponentsChat/GeneralView/GeneralView";
import WrapperUserProfile from "@/components/UI/WrapperUserProfile";
import React from "react";

const page = () => {
    return (
        <WrapperUserProfile>
            <GeneralView/>
        </WrapperUserProfile>
    )
}

export default page;