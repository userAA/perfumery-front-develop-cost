"use client";

import ProductCard from "@/components/ProductCard/ProductCard";
import {getAllAnnouncement, getAnnouncementsById} from "../../../store/actions/announcementsActions";
import {useAppDispatch, useAppSelector} from "../../../store/hooks/redux";
import {useParams} from "next/navigation";
import {useLayoutEffect} from "react";
import { getAlertTitleUtilityClass } from "@mui/material";


const page = () => {
    const dispatch = useAppDispatch();
    const {announcementById, announcementsItems} = useAppSelector((state) => state.announcements);

    const params = useParams();
    const {id} = params;

    useLayoutEffect(() => {
        if (announcementsItems.length == 0)
        {
            dispatch(getAllAnnouncement(20,0));
        }
        if (typeof id == "string")
        {
            dispatch(getAnnouncementsById(id));
        }
    }, []);
 
    return (
        <div>
            <ProductCard
                announcement={announcementById}
            />
        </div>
    )
}

export default page;