"use client";
import Link from "next/link";
import React from "react";
import Image from "next/image";
import img404 from "../../../public/404.png";
import "../404.scss";

const page = () => {
    return (
        <div className="page404" style={{}}>
            <div className="page404__menu">
                <h1>400</h1>
                <p>страница не найдена</p>
                <Link href={"/"}>перейти на главную</Link>
            </div>

            <div className="page404__img">
                <Image src={img404} alt=""></Image>
            </div>
        </div>
    )
}

export default page;