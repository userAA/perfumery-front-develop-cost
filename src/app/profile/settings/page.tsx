"use client";
import Delivery from "@/components/Delivery/Delivery";
import PersonalData from "@/components/PersonalData/PersonalData";
import Safety from "@/components/Safety/Safety";
import Wrapper from "@/components/UI/Wrapper";
import React, {useEffect, useState} from "react";
import {useAppDispatch, useAppSelector} from "../../../store/hooks/redux";
import { getProfile } from "@/store/actions/profileActions";
import { userAPI } from "@/api/api";
import ButtonFilter from "@/components/UI/ButtonFliter";
import Loader from "@/components/UI/Loader";

const page = () => {
    const [phone, setPhone] = useState("");
    const [email, setEmail] = useState("");
    const [firstname, setFirstname] = useState("");
    const [lastname, setLastname] = useState("");

    const [city, setCity]       = useState("");
    const [address, setAddress] = useState({index: "", street: "", house: "", apartment: "", country: ""});

    const [loading, setLoading] = useState<boolean>(false);
    const [flag, setFlag] = useState(false);

    const {errorUser, loadingUser, user} = useAppSelector((state) => state.profile);
    const {userId}                       = useAppSelector((state) => state.auth);

    const dispatch = useAppDispatch();

    useEffect(() => {
        dispatch(getProfile());
    }, []);

    useEffect(() => {
        if (user && !loadingUser && !errorUser)
        {
            setEmail(user.email);
            setPhone(user.phone);
            setFirstname(user.firstname);
            setLastname(user.lastname);

            setCity(user.city);
            user?.address && setAddress(user?.address);
        }

        setTimeout(() => { setFlag(!flag); }, 1000)
    }, [user]);

    const checkError = () => {
        if (!firstname) return "Имя не указано";
        if (!lastname) return "Фамилия не указана";
        if (!email) return "Почта не указана";
        return "";
    }

    const updateUser = async () => {
        try {
            const error = checkError();
            if (error) {
                console.log(error);
                return;
            }
            setLoading(true);
            const userData = {email, firstname, lastname, phone, city, address};
            await userAPI.updateUser(userData);
            setLoading(false);
            typeof window !== "undefined" && window.location.replace("/profile/"+userId);
        } catch (e: any) {
            console.log(e.message);
        }
    }

    return (
        <Wrapper>
            <PersonalData
                setEmail={setEmail}
                setPhone={setPhone}
                setLastname={setLastname}
                setFirstname={setFirstname}
                email={email}
                phone={phone}
                lastname={lastname}
                firstname={firstname}
            />

            <Delivery
                city={city}
                setCity={setCity}
                address={address}
                setAddress={setAddress}
            />
            <Safety/>
            <div style={{width: "60%", margin: "0 auto 100px auto"}}>
                <ButtonFilter onClickChildren={() => updateUser()}>
                    {loading ? <Loader/> : "save"}
                </ButtonFilter>
            </div>
        </Wrapper>
    )
}

export default page;