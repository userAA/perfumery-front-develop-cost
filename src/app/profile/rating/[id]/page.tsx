import React from "react";
import TopBarRating from "@/components/TopBarRating.tsx/TopBarRating";
import Wrapper from "@/components/UI/Wrapper";
import WrapperUserProfile from "@/components/UI/WrapperUserProfile";

const page = () => {
    return (
        <Wrapper>
            <WrapperUserProfile>
                <TopBarRating/>
            </WrapperUserProfile>
        </Wrapper>
    )
}

export default page;