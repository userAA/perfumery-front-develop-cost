import CreateAnnouncement from "@/components/CreateAnnouncement/CreateAnnouncement";
import Wrapper from "@/components/UI/Wrapper";
import React from "react";

const page = () => {
    return (
        <Wrapper>
            <CreateAnnouncement/>
        </Wrapper>
    )
}

export default page;