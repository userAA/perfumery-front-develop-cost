"use client"
import React from "react"
import { useParams } from "next/navigation"
import ProfileHOK from "@/HOK/ProfileHOK"

const page = () => {
    const params = useParams();
    const {id}   = params;

    return <ProfileHOK id={id}/>
}

export default page;