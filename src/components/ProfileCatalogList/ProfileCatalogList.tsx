"use client"
import React from "react";
import css from "./ProfileCatalogList.module.scss";
import {useAppDispatch, useAppSelector} from "../../store/hooks/redux";
import { useEffect } from "react";
import { getAllAnnouncement } from "@/store/actions/announcementsActions";

const ProfileCatalogList = ({children, userId}) => {
    const dispatch = useAppDispatch();
    const {announcementsItems} = useAppSelector((state) => state.announcements)

    const status = children === "Активные" ? "draft" : children === "Закрытые" && "disabled";

    useEffect(() => {
        dispatch(getAllAnnouncement(20, 0));
    }, []);

    return (
        <div className={css.profileCatalogList}>
            <div style={{marginTop: 30}}/>
            <div className={css.profileCatalogList__heading}>
                <p style={{color: "#000"}}>{children}</p>{" "}
                <p>({announcementsItems.filter((el) => el.status === status).filter((el) => el.user.id === userId).length})</p>
            </div>
        </div>
    )
}

export default ProfileCatalogList;