"use client";
import css from "./Footer.module.scss";
import Link from "next/link";
import Logo from "../../svgComponents/Logo";
import WhatsappIcon from "@/svgComponents/WhatsappIcon";
import MessageIcon from "@/svgComponents/MessageIcon";
import { useEffect, useState } from "react";
import LogoSmall from "../../svgComponents/LogoSmall";

const Footer = () => {

    const [windowWidth, setWindowWidth] = useState(0);

    useEffect(() => {
        setWindowWidth(typeof window !== "undefined" ? window.innerWidth : 1024)
    }, [])

    return (
        <footer>
            <div className={css.footer}>
                <div className={css.footer__container}>
                    <div className={css.footer__item}>
                        {windowWidth >= 1024 ? <Logo/> : <LogoSmall/>}
                        <div className={css.footer__info}>
                            <Link href={"tel:89067638061"} type="tel">
                                8 906 763 80 61
                            </Link>
                        </div>
                        <div className={css.footer__icons}>
                            <WhatsappIcon/>
                            <MessageIcon/>
                        </div>
                    </div>
                    <div className={css.footer__item}>
                        <h2>Компания</h2>
                        <Link href={"/"}>
                            <h3>Политика обработки персональных данных</h3>
                        </Link>
                        <Link href={"/"}>
                            <h3>Контакты</h3>
                        </Link>
                    </div>
                    <div className={css.footer__item}>
                        <h2>FAQ</h2>
                        <Link href={"/"}>
                            <h3>Заказы и доставка</h3>
                        </Link>
                        <Link href={"/"}>
                            <h3>Написать в WhatsApp</h3>
                        </Link>
                    </div>
                </div>
            </div>
        </footer>
    )    
}

export default Footer;