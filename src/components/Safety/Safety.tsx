import React , {useState} from "react";
import css from "./Safety.module.scss";
import InputProfile from "../UI/InputProfile";
import WrapperUserProfile from "../UI/WrapperUserProfile";

const Safety = () => {
    const [password, setPassword] = useState(''); 
    const [error,    setError]    = useState('');    
    const [success,  setSuccess]  = useState(''); 

    return (
        <WrapperUserProfile>
            <div className={css.safety}>
                <h2>{'safety'}</h2>
                <InputProfile value={password} setValue={setPassword} title={'password'} type={'password'}/>
                <div className={css.safety__password}>
                    <span></span>
                    <p>{'changePassword'}</p>
                    <p>{error ? error : success}</p>
                </div>
            </div>
        </WrapperUserProfile>
    )
}

export default Safety;