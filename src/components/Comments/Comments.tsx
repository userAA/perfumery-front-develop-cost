"use client";
import React from "react";
import css from "./Comments.module.scss";
import Image from "next/image";
import InputRating from "../../components/UI/MUI/InputRating";
import {motion} from "framer-motion";

interface IComments {
    description: string;
    firstname: string;
    lastname: string;
}

const Comments = ({description, firstname, lastname}: IComments) => {
    const listRatingVariants = {
        visible: () => ({opacity: 1, transition: {delay: 3*0.5}}),
        hidden: {opacity: 0}
    };    

    return (
        <motion.div
            variants={listRatingVariants}
            initial="hidden"
            animate="visible"
            className={css.comment}
        >
            <div className={css.comment__info}>
                <div className={css.comment__avatar}>
                    <Image width={30} height={30} alt="aa" src="/previewPhoto.png"/>
                    <p>
                        {firstname} {lastname}
                    </p>
                </div>
                <InputRating className={""} type={"secondary"}/>
            </div>
            <p>
                {description}
            </p>
        </motion.div>
    )    
}

export default Comments;