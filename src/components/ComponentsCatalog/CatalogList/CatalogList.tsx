"use client";
import React from "react";
import CatalogItem from "../CatalogItem/CatalogItem";
import classes from "./CatalogList.module.scss";

const CatalogList = ({items}:{items?:any}) => {
    return (
        <div className={classes.catalogList}>
            {items?.map((item, index) => (
                <CatalogItem item={item}/>
            ))}
        </div>
    )
}

export default CatalogList;