import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import CatalogList from "../CatalogList/CatalogList";
import "./Catalog.scss";
import Button from "@/components/UI/Button";

const Catalog = ({type, items, count, limit, setLimit, seeAll= true}:
    {type?: any; items?: any; count?: any; limit?: any; setLimit?: any; seeAll?: any}) => {


    if (type) 
    {
        return (
            <div
                className={type === "main" ? `catalogMain` : `catalogSecondary`}
                style={{marginTop: 24}}
            >
                <CatalogList items={items}></CatalogList>
                <div style={{marginBottom: 72}}>
                    {type? (
                        type !== "secondary" && seeAll ? (
                            <Button type={"secondary"}>
                                <span
                                    className={"catalogMainBtn"}
                                    onClick={() => limit < count && setLimit(count)}
                                >
                                    shoreMore <ExpandMoreIcon/>
                                </span>
                            </Button>
                        ): null
                    ): (
                        <Button type={"secondary"}>
                            <span
                                className={"catalogMainBtn"}
                                onClick={() => limit < count && setLimit(count)}
                            >
                                shoreMore <ExpandMoreIcon/>
                            </span>
                        </Button>
                    ) }
                </div>
            </div>
        )
    }
    else
    {
        return <div>error</div>
    }
}

export default Catalog;