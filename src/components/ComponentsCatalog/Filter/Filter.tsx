"use client";
import FilterCatalogList from "@/components/FilterCatalogList/FilterCatalogList";
import FilterModal from "../../FilterModal/FilterModal";
import Wrapper from "@/components/UI/Wrapper";
import { setShowFilter } from "@/store/actions/announcementsActions";
import { useAppDispatch, useAppSelector } from "@/store/hooks/redux";
import {motion} from "framer-motion";
import  Modal from "react-modal";
import css from "./Filter.module.scss";

const Filter = ({count}) => {
    const dispatch = useAppDispatch();
    const {showFilter} = useAppSelector((state) => state.announcements);

    const customStyles = {
        overlay: {
            background: "rgba(0, 0, 0, 0.55)"
        }
    };

    const openModal = () => {
        dispatch(setShowFilter(true));
    }

    const closeModal = () => {
        dispatch(setShowFilter(false));
    }

    return (
        <Wrapper>
            <div className={css.filter}>
                <div className={css.filter__menu}>
                    <motion.button
                        whileHover={{scale: 0.95}}
                        whileTap={{scale: 0.85}}
                        onClick={openModal}
                    >
                        <svg
                            fill="#000000"
                            width="20px"
                            height="20px"
                            viewBox="0 0 24 24"
                            xmlns="http://www.w3.org/2000/svg"
                        >
                            <path d="M1,4A1,1,0,0,1,2,3H12a1,1,0,0,1,0,2H2A1,1,0,0,1,1,4ZM22,3H17V2a1,1,0,0,0-2,0V6a1,1,0,0,0,2,0V5h5a1,1,0,0,0,0-2ZM2,13H8a1,1,0,0,0,0-2H2a1,1,0,0,0,0,2Zm20-2H13V10a1,1,0,0,0-2,0v4a1,1,0,0,0,2,0V13h9a1,1,0,0,0,0-2ZM2,21H15a1,1,0,0,0,0-2H2a1,1,0,0,0,0,2Zm20-2H20V18a1,1,0,0,0-2,0v4a1,1,0,0,0,2,0V21h2a1,1,0,0,0,0-2Z" />
                        </svg>
                        filters
                    </motion.button>
                    <Modal
                        closeTimeoutMS={300}
                        isOpen={showFilter}
                        style={customStyles}
                        className={css.modal}
                        onRequestVlode={closeModal}
                    >
                        <FilterModal closeModal={closeModal}/>
                    </Modal>
                </div>
                <p className={css.filter__searchResults}>
                    {count > 0 ? `${"total"}: ${count}` : "Nothing found"}{" "}
                </p>
                <div>
                    <FilterCatalogList>{"sort"}</FilterCatalogList>
                </div>
            </div>
        </Wrapper>
    )
}

export default Filter;