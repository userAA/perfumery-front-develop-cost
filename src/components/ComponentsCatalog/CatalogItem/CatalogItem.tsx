"use client"
import React, {useEffect, useState} from "react"
import css from "./CatalogItem.module.scss";
import Link from "next/link";
import {Carousel} from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { useRouter } from "next/navigation";
import FavouritesSvg from "@/svgComponents/FavoriteSvg";
import MenuIcon from "@/svgComponents/MenuIcon";

const CatalogItem = ({item}: any) => {
    const router = useRouter();
    const [date, setDate] = useState("");

    useEffect(() => {
        const formatDateTime = (isoDate: string) => {
            const date  = new Date(isoDate);
            const today = new Date();
            const yesterday = new Date(today);
            yesterday.setDate(yesterday.getDate() - 1);

            if (date.toDateString() === today.toDateString())
            {
                const hours = date.getHours();
                const minutes = date.getMinutes();
                return `Сегодня в ${hours}:${minutes < 10 ? "0": ""}${minutes}`;
            }
            else if (date.toDateString() === yesterday.toDateString())
            {
                const hours = date.getHours();
                const minutes = date.getMinutes();
                return `Вчера в ${hours}:${minutes < 10 ? "0": ""}${minutes}`;
            }
            else
            {
                return `${date.getDate() >= 10 ? date.getDate() : `0${date.getDate()}`}.
                        ${date.getMonth() + 1 >= 10 ? date.getMonth() + 1 : `0${date.getMonth() + 1}`}
                        ${date.getHours()}:${date.getMinutes()}`;
            }
        }
        setDate(formatDateTime(item?.created));
    }, []);

    return (
        <div>
            <div className={css.catalogItem}>
                <div className={css.catalogItem__preview}>
                    <Carousel
                        showStatus={false}
                        showThumbs={false}
                        showArrows={false}
                        showIndicators={item?.images?.length > 1}
                        onClickItem={() => router.push(`/product/${item?.id}`)}
                    >
                        {item?.images?.map((el: string, index: number) => (
                            <img className={css.carouselItem} src={el}/>
                        ))}
                    </Carousel>
                </div>
                <Link href={`/product/${item?.id}`}>
                    <div className={css.catalogItem__params}>
                        <div className={css.catalogItem__params__titleBlock}>
                            <div className={css.catalogItem__params__title}>
                                {item?.brand}
                            </div>
                            <div className={css.catalogItem__params__priceTitle}>
                                {item?.price} ₽
                            </div>
                            <div className={css.catalogItem__params__rewiewTitle}>
                                4.4 *****
                            </div>
                            <div className={css.catalogItem__params__subtitle}>
                                {item?.city}
                            </div>
                        </div>
                        <div className={css.catalogItem__params__infoBlock}>
                            <FavouritesSvg/>
                            <div
                                style={{marginBottom: 24, marginTop: 20, position: "relative", zIndex: 1000}}
                                onClick={(e) => {e.preventDefault(); e.stopPropagation();}}
                            >
                                <MenuIcon/>
                            </div>
                            <div className={css.carouselItem__params__subtitle}>
                                {item?.user?.firstname + " " + item?.user?.lastname}
                            </div>
                            <div className={css.catalogItem__params__dateTitle}>{date}</div>
                        </div>
                    </div>
                </Link>
            </div>
        </div>
    )
}

export default CatalogItem;