"use client";
import Link from "next/link";
import Wrapper from "../UI/Wrapper";
import css from "./Header.module.scss"

import Auth  from "../Auth/Auth";
import Confirmation from "../Auth/Confirmation";
import Email from "../Auth/Email";
import Regist from "../Auth/Regist";

import En from "@/svgComponents/en.png";
import Fr from "@/svgComponents/fr.png";
import Ru from "@/svgComponents/ru.png";

import { setFilterBottleType,  setFilterBrand, setFilterCity, setFilterIsNew, setFilterName, setFilterPrice} 
from "../../store/actions/announcementsActions"
import {changeTypeModal, closeModal, openModal, userLogout} from '../../store/actions/authActions'
import {useAppDispatch, useAppSelector} from "../../store/hooks/redux";
import Logo from "@/svgComponents/Logo";
import MessageSvg from "@/svgComponents/MessageSvg";
import SearchSvg from "@/svgComponents/SearchSvg";
import Image from "next/image";
import { useState } from "react";
import Modal from "react-modal";
import AnimationSvg from "../UI/AnimationSvg";
import UserSvg from "@/svgComponents/UserSvg";
import FavouritesSvg from "@/svgComponents/FavoriteSvg";

const Header = () => {
    const dispatch = useAppDispatch();
    const [lang, setLang] = useState("ru");
    const [cssDisplay, setCssDisplay] = useState("none");

    function openModalSearch() {

    }

    const {isAuth, stepModal, modalIsOpen, userId} = useAppSelector(
        (state) => state.auth
    )

    const onClickOpenModal = () => {
        dispatch(changeTypeModal("auth"));
        dispatch(openModal());
    }

    const onClickCloseModal = () => {
        dispatch(changeTypeModal("auth"));
        dispatch(closeModal());
    }

    const customStyles = {
        overlay: {
            background: "rgba(0,0,0,0.55)"
        },
        content: {
            top: "50%",
            left: "50%",
            right: "auto",
            bottom: "auto",
            padding: "10px",
            background: "none",
            border: "none",
            overflow: "hidden",
            transform:  "translate(-50%, -50%)",
        }
    }

    const getModalContent = () => {
        switch (stepModal) {
            case "auth":   return <Auth/>
            case "email":  return <Email/>
            case "regist": return <Regist/>
            case "code":   return <Confirmation/>
            default:       return <Auth/>
        }
    }

    const logout = () => {
        dispatch(userLogout());
    }

    const onSetLang = (lang: string): void => {
        setLang(lang);

        typeof window !== "undefined" && window.localStorage.setItem("lang", lang);
        handleDropDown();
    }
 
    const handleDropDown = () => {
        if (cssDisplay === "none")
        {
            setCssDisplay("block");
        }
        else
        {
            setCssDisplay("none");
        }
    }

    return (
        <header className={css.header}>
            <Modal
                isOpen= {modalIsOpen}
                ariaHideApp={false}
                style={customStyles}
                onRequestClose={onClickCloseModal}
            >
                {getModalContent()}
            </Modal>
            <Wrapper>
                <div className={css.headerDesktop}>
                    <div className={css.headerDesktop__menu}>
                        <div className={css.headerDesktop__item}>
                            <Link
                                href="/"
                                onClick={() => {
                                    dispatch(setFilterPrice([0, 100000]));
                                    dispatch(setFilterBrand(""));
                                    dispatch(setFilterCity(""));
                                    dispatch(setFilterName(""));
                                    dispatch(setFilterIsNew(""));
                                    dispatch(setFilterBottleType(""));
                                }}
                            >
                                <p>
                                    <Logo/> 
                                </p>
                            </Link>
                        </div>
                        <div className={css.headerDesktop__item}>
                            <span style={{cursor: "pointer"}}>Продукция</span>
                            <span style={{cursor: "pointer"}}>Отзывы</span>
                            <span style={{cursor: "pointer"}}>Контакты</span>
                        </div>
                        <div className={css.headerDesktop__item}>
                            <div>
                                <div onClick={handleDropDown} style={{cursor: "pointer"}}>
                                    {lang === "ru" ? (
                                        <Image src={Ru} alt="ru" className={css.lang}/>
                                    ) : lang == "us" ? (
                                        <Image src={En} alt="us" className={css.lang}/>
                                    ) : (
                                        lang === "fr" && (
                                            <Image src={Fr} alt="fr" className={css.lang}/>
                                        )
                                    )}
                                </div>
                                <ul
                                    className={css.dropdown}
                                    style={{cursor: "pointer", display: cssDisplay}}
                                >
                                    <li onClick={() => onSetLang("fr")}>
                                        <Image src={Fr} alt="fr" className={css.langSelect}/>
                                        <span style={{fontWeight: "500", fontSize: 18, lineHeight: "21.78px", color: "£27213C"}}>FR</span>
                                    </li>
                                    <li onClick={() => onSetLang("us")}>
                                        <Image src={En} alt="us" className={css.langSelect}/>
                                        <span style={{fontWeight: "500", fontSize: 18, lineHeight: "21.78px", color: "£27213C"}}>USA</span>
                                    </li>
                                    <li onClick={() => onSetLang("ru")}>
                                        <Image src={Ru} alt="ru" className={css.langSelect}/>
                                        <span style={{fontWeight: "500", fontSize: 18, lineHeight: "21.78px", color: "£27213C"}}>RUS</span>
                                    </li>
                                </ul>
                            </div>
                            {isAuth ? (
                                <div style={{display: "flex", flexDirection: "row"}}>
                                    <div className={css.headerDesktop__item}>
                                        <AnimationSvg>
                                            {userId && isAuth ? (
                                                <Link href={`/messages`}>
                                                    <MessageSvg/>
                                                </Link>
                                            ): (
                                                <MessageSvg/>
                                            )}
                                        </AnimationSvg>

                                        <AnimationSvg>
                                            {userId && isAuth ? (
                                                <Link href={`/profile/${userId}`}>
                                                    <UserSvg/>
                                                </Link>
                                            ) : (
                                                <UserSvg/>
                                            )}
                                        </AnimationSvg>

                                        <AnimationSvg>
                                            <Link href="/">
                                                <FavouritesSvg/>
                                            </Link>
                                        </AnimationSvg>

                                        <AnimationSvg>
                                            <Link onClick={openModalSearch} href="/">
                                                <SearchSvg/>
                                            </Link>
                                        </AnimationSvg>

                                        {userId && isAuth ? (
                                            <AnimationSvg>
                                                <button
                                                    style={{background: "none", border: "none", fontSize: "20px", color: "#000"}}
                                                    onClick={logout}
                                                >
                                                    exit
                                                </button>
                                            </AnimationSvg>
                                        ) : null}
                                    </div>    
                                </div>    
                            ): (
                                <button onClick={onClickOpenModal}>Присоедениться</button>
                            )}
                        </div>
                    </div>
                </div>
            </Wrapper>
        </header>
    )
}

export default Header;