"use client";
import "../../style/globals.scss";
import "../../style/ui.scss";
import React, {useEffect} from "react";
import css from "./TopBarRating.module.scss";
import InputRating from '../UI/MUI/InputRating';
import { useAppDispatch, useAppSelector } from "@/store/hooks/redux";
import { getProfile } from "@/store/actions/profileActions";
import Comments from "../Comments/Comments";

const TopBarRating = () => {
    const dispatch = useAppDispatch();
    const {user}   = useAppSelector((state) => state.profile);

    useEffect(() => {
        dispatch(getProfile());
    }, []);

    return (
        <>
            <div className={css.topBarRating}>
                <div className={css.topBarRating__text}>
                    <h2>{"sellerReviews"}</h2>
                    <p>{user.firstname} {user.lastname}</p>
                </div>
                <InputRating type={"primary"}/>
            </div>
            <Comments/>
        </>
    )
}

export default TopBarRating;
