import React from "react";
import "../../style/globals.scss";
import "../../style/ui.scss";
import css from './PersonalData.module.scss';
import InputProfile  from "../../components/UI/InputProfile";
import classNames from "classnames";
import WrapperUserProfile from "../UI/WrapperUserProfile";
import Avatar from "../UI/Avatar";

interface IPersonalData {
    email:     string;
    phone:     string;
    firstname: string;
    lastname:  string;
    setEmail: React.Dispatch<React.SetStateAction<string>>;
    setPhone: React.Dispatch<React.SetStateAction<string>>;
    setFirstname: React.Dispatch<React.SetStateAction<string>>;
    setLastname: React.Dispatch<React.SetStateAction<string>>; 
}

const PersonalData: React.FC<IPersonalData> = ({
    email,
    phone,
    firstname,
    lastname,
    setEmail,
    setPhone,
    setFirstname,
    setLastname
}) => {
    return (
        <WrapperUserProfile>
            <div className={css.personalData}>
                <Avatar/>
                <div
                    className={classNames(css.personalData__info, css.personalData__item)}
                >
                    <h2>{"personalData"}</h2>
                    <InputProfile value={firstname} title={"name"} setValue={setFirstname} type={"text"}/>
                    <InputProfile value={lastname} title={"surname"} setValue={setLastname} type={"text"}/>
                    <InputProfile value={phone} title={"phone"} setValue={setPhone} type={"text"}/>
                    <InputProfile value={email} title={"mail"} setValue={setEmail} type={"text"}/>
                </div>
            </div>
        </WrapperUserProfile>        
    )
}

export default PersonalData;