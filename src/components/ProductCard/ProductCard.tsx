import FavouritesSvg from "@/svgComponents/FavoriteSvg";
import css from "./ProductCard.module.scss";
import {useEffect, useState} from "react";
import Catalog from "../ComponentsCatalog/Catalog/Catalog";
import { useAppDispatch, useAppSelector } from "@/store/hooks/redux";
import { getAllAnnouncement } from "@/store/actions/announcementsActions";

const ProductCard = ({
    announcement
}: {
    announcement?: any
}) => {
    const {id, images, name, isNew, brand, volume, price, description, user, bottleType, city} = announcement;

    const dispatch = useAppDispatch();
    const {announcementsItems, count, filter} = useAppSelector((state) => state.announcements);
    const [limit, setLimit] = useState(20);

    useEffect(() => {
        dispatch(getAllAnnouncement(limit,0,filter));
    }, [limit, filter])

    if (Object.keys(announcement).length === 0) return <></>;

    return (
        <div>
            <div className={css.productCart}>
                <div className={css.productCart__previewImages}>
                    {images.map((item: any, index: any) => (
                        <div className={css.productCart__previewImages__imageContainer}>
                            <img src={item} className={css.productCart__previewImages__image}/>
                        </div>
                    ))}
                </div>

                <div className={css.productCart__imageContainer}>
                    <img src={images[0]} alt="" className={css.productCart__image}/>
                </div>

                <div className={css.productCart__params}>
                    <div
                        style={{display: "flex", justifyContent: "space-between", flexDirection: "column", height: "100%"}}
                    >
                        <div>
                            <div>
                                <div className={css.productCart__params__row}>
                                    <div>
                                        <div className={css.productCart__params__title}>
                                            {brand}
                                        </div>
                                        <div className={css.productCart__params__title}>{name}</div>
                                    </div>
                                    <FavouritesSvg/>
                                </div>
                                <div className={css.productCart__params__row}>
                                    <div className={css.productCart__params__row}>
                                        <div
                                            className={css.productCart__params__userName}
                                            style={{marginRight: 8}}
                                        >
                                            {user?.firstname + " " + user?.lastname}
                                        </div>
                                        <div className={css.productCart__params__date}>
                                            {new Date(announcement?.created).getDate() >= 10
                                            ? new Date(announcement?.created).getDate(): "0" + new Date(announcement?.created).getDate()}
                                            /
                                            {new Date(announcement?.created).getDate() + 1 >= 10
                                            ? new Date(announcement?.created).getDate() + 1 : "0" + new Date(announcement?.created).getDate()}
                                            /{new Date(announcement?.created).getFullYear()}   
                                        </div>
                                    </div>  
                                    <div className={css.productCart__params__rewiew}>
                                        4.0 *****
                                    </div>  
                                </div>
                            </div>
                            <hr
                                style={{color: "#27213C", height: 1.5, backgroundColor: "#27213C", marginLeft: -30, marginRight: -30}}
                            />
                            <div
                                style={{display: "flex", flexDirection: "column", gap: 24, marginBottom: 24, marginTop: 24}}
                            >
                                <div className={css.productCart__params__row}>
                                    <div className={css.productCart__params__subtitle}>
                                        Флакон
                                    </div>
                                    <div className={css.productCart__params__subtitle__desc}>
                                        {volume}
                                            ml
                                    </div>
                                </div>
                                <div className={css.productCart__params__row}>
                                    <div className={css.productCart__params__subtitle}>
                                        Состояние
                                    </div>
                                    <div className={css.productCart__params__subtitle__desc}>
                                        {JSON.parse(isNew) ? "isNes" : "noNew"}
                                    </div>
                                </div>
                                <div className={css.productCart__params__row}>
                                    <div className={css.productCart__params__subtitle}>Адрес</div>
                                    <div className={css.productCart__params__subtitle__desc}>
                                        {city}
                                    </div>
                                </div>
                            </div>
                            <div className={css.productCart__params__subtitle}>Описание</div>
                            <div className={css.productCart__params__subtitle__descript}>
                                {description}
                            </div>
                        </div>
                        <div className={css.productCart__params__row}>
                            <div className={css.productCart__params__price}>{price}₽</div>
                            <button className={css.productCart__params__buy}>Купить</button>
                        </div>
                    </div>
                </div> 

            </div>
            <div className={css.title__container}>
                <div className={css.title}>Похожие товары</div>
                <Catalog
                    type={"main"}
                    items={announcementsItems}
                    count={count}
                    limit={limit}
                    setLimit={setLimit}
                />
            </div>
        </div>
    )   
}

export default ProductCard;