"use client";
import "../../style/globals.scss"
import {adminApi, announcementAPI } from "@/api/api";
import Avatar from "../../components/UI/Avatar";
import Loader from "../../components/UI/Loader";
import CreateAnnouncementDropDown from "@/components/UI/MUI/CreateAnnouncementDropDown";
import { IBrand, IFormat, IPerfume, IState } from "@/models/announcements.model";
import Basket from "@/svgComponents/Basket";
import Editing from "@/svgComponents/Editing";
import {Alert, Box} from "@mui/material";
import {motion} from "framer-motion";
import { ChangeEvent, useEffect, useState } from "react";
import ButtonFilter from "../UI/ButtonFliter";
import ButtonSecondary from "../UI/ButtonSecondary";
import TextArea from "../UI/TextArea";
import WrapperUserProfile from "../UI/WrapperUserProfile";
import css from "./CreateAnnouncement.module.scss";
import i18next from "i18next";

const CreateAnnouncement = () => {
    const [loading, setLoading]         = useState<boolean>(false);
    const [description, setDescription] = useState<string>("");

//
    const [file, setFile]             = useState<any[]>([]);
    const [fileResult, setFileResult] = useState<any>("");

    const handleFileChange = (event: ChangeEvent<HTMLInputElement> | any) => {
        let reader = new FileReader();
        reader.onload = function(event: any) {
            setFileResult(event.target.result);
        };

        if (event.target.files.length) {
            reader.readAsDataURL(event.target.files[0]);
            file.length !== 10 && setFile([...file, event.target.files[0]]);
            console.log(event.target.files[0]);
        }           
    }
    
    const handleDeleteImage = (image: any) => {
        const newFiles = file.filter((element) => element !== image);
        setFile(newFiles);           
    }    

//    
    const [brands, setBrands] = useState<Array<IBrand>>([]);
    const [brandValue, setBrandValue] = useState<IBrand | null>(null);
    const [brandInputChange, setBrandInputChange] = useState<string>("");

    const getBrands = async () => {
        const res = await announcementAPI.getAllBrands(10, 0, brandInputChange);
        setBrands(res.data.data);
    }

    useEffect(() => {
        if (brandInputChange) getBrands();
        else setBrands([]);
    }, [brandInputChange]);

    //
    const [perfumes, setPerfumes] = useState<Array<IPerfume>>([]);
    const [perfumeValue, setPerfumeValue] = useState<IPerfume | null>(null);
    const [perfumeChange, setPerfumeChange] = useState<string>("");

    const getPerfumes = async () => {
        if (brandValue) {
            const res = await announcementAPI.getAllPerfumes(10, 0, brandValue.id, perfumeChange);
            setPerfumes(res.data.data);
        }
    }

    useEffect(() => {
        if (perfumeChange) getPerfumes();
        else setPerfumes([]);
    }, [perfumeChange])

    //
    const formats: Array<IFormat> = [
        {name: "bottle"},
        {name: "sampler"},
        {name: "tester"},
        {name: "otlivant"},
        {name: "set"}
    ];
    const [format, setFormat] = useState<IFormat | null>(null);

    //
    const stateItems: Array<IState> = [{name: "new"}, {name: "used"}];
    const [state, setState]         = useState<IState | null>(null);

    //
    const [price, setPrice] = useState<string>("");

    //
    const [volume, setVolume] = useState<string>("");

    //
    const countries = ([
        {id: "france_fr", name: "france", code: "FR"},
        {id: "russia_ru", name: "russia", code: "RU"},
        {id: "usa_en", name: "usa", code: "US"}
    ]);
    const [country, setCountry] = useState<any | null>({test: "name"});

    //
    const [inputCity, setInputCity] = useState<string>("");
    const [cities, setCities]       = useState<Array<IBrand> | null>([]);
    const [city, setCity]           = useState<any | null>({test: "name"});

    const getCities = async (city: string="") => {
        const {data} = await adminApi.getCities(i18next.language, country.code, city);
        return data;
    }

    useEffect(() => {
        (async () => {
            const data = await getCities();
            const preCities = data.map((item:any, index: number) => {return {id: index, name: item.name};})
            const cities = Array.from(new Set(preCities.map((item: any) => item.name)))
            .map((name) => preCities.find((item: any) => item.name === name))
            setCities(cities);
        })();
    }, [country]);

    useEffect(() => {
        (async () => {
            const data = await getCities(inputCity);
            const preCities = data.map((item: any, index: number)=> {return{id: index, name: item.name}});
            const cities = Array.from(new Set(preCities.map((item: any) => item.name)))
            .map((name) => preCities.find((item: any) => item.name === name));
            setCities(cities);
        })();
    }, [inputCity])

    //
    const [errors, setErrors] = useState<string[]>([]);

    const checkError = () => {
        let temp = [];
        if (!brandValue) temp.push("Выберите бренд");
        if (!perfumeValue) temp.push("Выберите наименование");
        if (!format) temp.push("Выберите формат");
        if (!state) temp.push("Выберите состояние");
        if (!price || isNaN(+price)) temp.push("Добавьте цену");
        if (!volume || isNaN(+volume)) temp.push("Добавьте обЪем");
        if (!description) temp.push("Добавьте описание");

        if (!file.length) temp.push("Добавьте изображение");
        const fileSize = file.reduce((s, i) => (s = s + i.size), 0);
        if (file.length && fileSize > 6250000) temp.push("Изображения слишком большие");

        if (!city) temp.push("Выберите город");
        if (!country) temp.push("Выберите страну");

        setErrors(temp);
        const preTemp = temp;
        temp = [];
        return preTemp;
    }

    const postAnnouncement = async () => {
        try {
            const errors: any = checkError();
            if (errors.length)
            {
                setErrors(errors);
                return;
            }
            else
            {
                console.log(file);
                setLoading(true);

                const formData = new FormData();
                formData.append("name", perfumeValue.name);
                formData.append("description", description);
                formData.append("price", price);
                formData.append("brand", brandValue.name);
                formData.append("state", state.name === "Новое");
                formData.append("bottleType", format.name);
                formData.append("volume", volume);
                for (const i of file) {
                    formData.append("image", i);
                }
                formData.append("city", city.name);
                formData.append("country", country.name);

                const newAnnouncement: any = await announcementAPI.createCard(formData);
                setErrors([]);
                setLoading(false);
                location.replace(location.origin + "/product/" + newAnnouncement.data?.data?.id);
            }
        } catch (event) {
            setLoading(false);
        }
    }

    return (
        <WrapperUserProfile>
            <div className={css.createAnnouncement}>
                <div className={css.createAnnouncement__topBar}>
                    <div
                        style={{
                            display: "flex",
                            flexDirection: "column",
                            alignItems: "center",
                            justifyContent: "center",
                            width: "100%",
                        }}
                    >
                        <motion.div
                            initial={{
                                scale: 1,
                                outline: "none",
                                display: "flex",
                                justifyContent: "center",
                                alignItems: "center",
                                maxHeight: "350px",
                                width: "100%"
                            }}
                            whileHover = {{scale: 0.98, outline: "none"}}
                            whileTap   = {{scale: 0.95, outline: "none"}}
                        >
                            <label
                                htmlFor="photo"
                                style={{width: "100%"}}
                                className={css.createAnnouncement__label}
                            >
                                <Avatar/>
                            </label>
                            <input
                                className={css.createAnnouncement__inputFile}
                                onChange={handleFileChange}
                                type="file"
                                id="photo"
                            />
                        </motion.div>
                        {file.map((item) => (
                            <motion.img
                                initial={{scale: 1, outline: "none"}}
                                whileHover={{scale: 0.98, outline: "none"}}
                                whileTap={{scale: 0.95, outline: "none"}}
                                style={{width: 300, marginTop: 24}}
                                onClick={() => handleDeleteImage(item)}
                                src={URL.createObjectURL(item)}
                            />
                        ))}
                    </div>
                    <div className={css.createAnnouncement__info}>
                        <h2>{"create Announcement"}</h2>
                        <CreateAnnouncementDropDown
                            brands={brands}
                            setBrandInputChange={setBrandInputChange}
                            setBrandValue={setBrandValue}
                            brand={brandValue}
                            perfumes={perfumes}
                            setPerfumeChange={setPerfumeChange}
                            setPerfumeValue={setPerfumeValue}
                            perfume={perfumeValue}
                            formats={formats}
                            setFormat={setFormat}
                            format={format}
                            stateItems={stateItems}
                            setState={setState}
                            state={state}
                            setPrice={setPrice}
                            price={price}
                            setVolume={setVolume}
                            volume={volume}
                            countries={countries}
                            setCountry={setCountry}
                            country={country}
                            cities={cities}
                            setCity={setCity}
                            city={city}
                            setInputCity={setInputCity}
                            inputCity={inputCity}
                        />
                    </div>  
                </div>     
                <div style={{display: "flex", flexDirection: "column", gap: "32px"}}>
                    <TextArea
                        title={"description"}
                        value={description}
                        setValue={setDescription}
                    />
                    <Box display="flex" flexWrap="wrap" gap={1}>
                        {errors.map((err, index) => (
                            <Alert variant="filled" severity="error">
                                {err}
                            </Alert>
                        ))}
                    </Box>

                    <div className={css.createAnnouncement__buttonGroup}>
                        <ButtonFilter
                            onClickChildren={() => {loading ? null : postAnnouncement()}}
                        >
                            {loading ? <Loader/> : "createListing"}
                        </ButtonFilter>
                        <ButtonSecondary type={"primary"}>
                            <Basket color={"#fff"}/>
                        </ButtonSecondary>
                        <ButtonSecondary type={"primary"}>
                            <Editing color={"#fff"}/>
                        </ButtonSecondary>
                    </div>
                </div>
            </div> 
        </WrapperUserProfile>
    )
}

export default CreateAnnouncement;