"use client"
import React, { useEffect, useRef, useState } from "react";
import classes from "./Admin.module.scss";
import {adminApi} from "../../api/api";
import * as echarts from 'echarts';

const Admin = () => {
    const [users, setUsers] = useState<Array<any>>([]);
    const chartRef = useRef<any>(null);
    const [chart, setChart] = useState(null);

    const getUsers = async () => {
        const users = await adminApi.getUsers().then(res => res);
        setUsers(users.data.data);
    }

    useEffect(() => {
        getUsers();
    }, [])

    useEffect(() => {
        if (typeof window !== "undefined")
        {
            let myChart:any;
            if (chart)
            {
                myChart = chart;
            }
            else
            {
                myChart = echarts.init(chartRef.current);
                window.onresize = function() {myChart.resize();};
                setChart(myChart);    
            }

            const months = ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'];
            const data   = [0,0,0,0,0,0,0,0,0,0,0,0];

            users.map(user => {
                const month  = new Date(user.created).getMonth();
                data[month] += 1;
            })

            const option = {
                tooltip: {
                    trigger: 'axis'
                },
                title: {
                    text: 'Регистрация пользователей'
                },
                grid: {
                    left: '3%',
                    right: '4%',
                    bottom: '3%',
                    containLabel: true
                },
                xAxis: {
                    type: 'category',
                    boundaryGap: false,
                    data: months
                },
                yAxis: {
                    type: 'value'
                },
                series: [
                    {
                        name: 'Users',
                        type: 'line',
                        data: data
                    }
                ]
            };

            myChart.setOption(option);
        }
    }, [users]);
    
    const blockUser = async (id:string) => {
        await adminApi.userBlock(id);
        await getUsers();
    }

    const unBlockUser = async (id:string) => {
        await adminApi.userUnBlock(id);
        await getUsers();
    }
    if (typeof window == "undefined") return null;

    return (
        <div className={classes.Admin}>
            <div className={classes.echarts} ref={chartRef}></div>
            <div>
                {users?.map(user => {
                    const date = new Date(user.created);
                    return (
                        <div key={user.id} className={classes.Admin__Row}>
                            <div className={classes.Admin__Row_Id}>{user.id}</div>
                            <div className={classes.Admin__Row_Date}>{date.toLocaleString('ru')}</div>
                            <div className={classes.Admin__Row_Firstname}>{user.firstname}</div>
                            <div className={classes.Admin__Row_Lastname}>{user.lastname}</div>
                            <div className={classes.Admin__Row_City}>{user.city}</div>
                            <div className={classes.Admin__Row_Phone}>{user.phone}</div>
                            <div className={classes.Admin__Row_Email}>{user.email}</div>
                            {user.isBlocked ? 
                            <div className={classes.Admin__Row_Block} onClick={() => unBlockUser(user.id)}>-</div>:
                            <div className={classes.Admin__Row_Block} onClick={() => blockUser(user.id)}>X</div>}
                        </div>    
                    )
                })}
            </div>
        </div>        
    )
}

export default Admin;