"use client"
import { IUserChatModel } from '@/models/chats.model';
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import { Button } from '@mui/material';
import { useState } from 'react';
import {useRouter} from 'next/navigation';
import ChatWithPeople from '../ChatWithPeople/ChatWithPeople';
import ListOfPeople from '../ListOfPeople/ListOfPeople';
import css from './GeneralView.module.scss';

interface IGeneralViewState {
    chatsList: IUserChatModel[]
    currentChat: IUserChatModel | {}
    viewType: 'list' | 'messages'
}

const GeneralView = () => {
    const [state, setState] = useState<IGeneralViewState>({
        chatsList: [],
        currentChat: {},
        viewType: 'list'
    });
    const router = useRouter();

    const setViewType = view => {
        setState({...state, viewType: view})
    }

    return (
        <>
            <Button onClick={() => router.back()} size='large' color='inherit'>
                <ArrowBackIosIcon/>
                Back
            </Button>
            <div className={css.generalView}>
                <ListOfPeople
                    chatsList = {state.chatsList}
                    viewType={state.viewType}
                />
                <ChatWithPeople 
                    setViewType={setViewType}
                    viewType={state.viewType}
                    currentChat={state.currentChat}
                />
            </div>
        </>
    )
}

export default GeneralView;