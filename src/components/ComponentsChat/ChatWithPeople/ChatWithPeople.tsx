"use client";
import React, {useState} from "react";
import css from "./ChatWithPeople.module.scss";
import Clip from "@/svgComponents/Clip";
import Send from "@/svgComponents/Send";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import {motion} from "framer-motion";
import Messages from "../Messages/Messages";
import ArrowBackIosIcon from "@mui/icons-material/ArrowBackIos";
import classNames from "classnames";
import "../../../style/globals.scss";

interface IChatWithPeopleState {
    inputValue: string;
};

const ChatWithPeople = ({
    currentChat,
    viewType,
    setViewType
}) => {

    const [state, setState] = useState<IChatWithPeopleState>({inputValue: ""});

    function handleChangeInput(event: React.ChangeEvent<HTMLInputElement>) {
        setState({...state, inputValue: event.target.value});
    }

    function handleSendButton() {
        console.log(`currentChat?.hasOwnProperty("recipient") ${currentChat?.hasOwnProperty("recipient")}`)
    }

    return (
        <div 
            className={classNames(
                css.chatWithPeople,
                css.chatWithPeople__disactive,
                css.chatWithPeople__active,
                {[css.view]: viewType !== 'messages'}
            )}
        >
            <div className={css.chatWithPeople__topBar}>
                <div className={css.chatWithPeople__buttonBack}>
                    <button>
                        <ArrowBackIosIcon sx={{color: "#000"}} onClick={() => setViewType("list")}/>
                    </button>
                </div>

                <motion.div
                    whileHover={{scale: 1.1}}
                    whileTap={{scale: 0.9}}
                    className={css.chatWithPeople__dropDown}
                >
                    <MoreVertIcon/>
                </motion.div>
            </div>

            <Messages/>

            <motion.div
                whileTap={{scale: 1.02}}
                className={css.chatWithPeople__bottomBar}
            >
                <input
                    value={state.inputValue}
                    onChange={handleChangeInput}
                    type="text"
                    placeholder={"writeMessage"}
                />
                <div className={css.buttonMessanger}>
                    <button>
                        <Clip/>
                    </button>
                    <button onClick={handleSendButton} className={css.send}>
                        <Send/> 
                    </button>
                </div>
            </motion.div>
        </div>
    )
}

export default ChatWithPeople;