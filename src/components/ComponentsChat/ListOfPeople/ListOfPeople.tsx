"use client"
import React from "react";
import css from "./ListOfPeople.module.scss";
import SearchSvg from "@/svgComponents/SearchSvg";
import {motion} from "framer-motion";
import classNames from "classnames";
import "../../../style/globals.scss";

const ListOfPeople = ({chatsList=[], viewType}) => {

    return (
        <div className={classNames({[css.listOfPeople]: 1}, {[css.view]: viewType !== 'list'})}>
            <div className={css.listOfPeople__menu}>
                <div className={css.listOfPeople__text}>
                    <h2>{'messages'}</h2>
                    <p>({chatsList.length})</p>
                </div>
                <motion.div
                    whileTap={{scale: 1.02}}
                    className={css.listOfPeople__search}
                >
                    <button>
                        <SearchSvg/>
                    </button>
                    <input type="search" placeholder="searchMessages"/>
                </motion.div>  
            </div>  
        </div>
    )
}

export default ListOfPeople;