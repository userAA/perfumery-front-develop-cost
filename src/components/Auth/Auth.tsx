import classes from "./Auth.module.scss"
import React, {useState, useEffect} from "react";
import {useAppDispatch, useAppSelector } from "@/store/hooks/redux";
import classNames from "classnames";
import InputMask from "react-input-mask";

import {changeTypeModal, userAuth} from '../../store/actions/authActions'
import EyeOpenIcon from "@/svgComponents/EyeOpenIcon";
import EyeCloseIcon from "@/svgComponents/EyeCloseIcon";
import LogoWhite from "@/svgComponents/LogoWhite"

const Auth = () => {
    const dispatch = useAppDispatch();
    const [hidePassword, setHidePassword] = useState(true);
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const {error, loading} = useAppSelector((state) => state.auth);
    const [lang, setLang]         = useState("ru");

    useEffect(() => {
        const lang = typeof window !== "undefined" && window.localStorage.getItem("lang");
        if (lang) setLang(lang);
    }, []);

    const toggleVisiblePassword = () => {
        setHidePassword(!hidePassword);
    }

    const onClickAuth = () => {
        dispatch(userAuth(username, password));
    }

    const onClickRegist = () => {
        dispatch(changeTypeModal("regist"));
    }

    const restorePass = () => {
        dispatch(changeTypeModal("email"))
    }

    return (
        <div className={classes.Auth}>
            <div className={classes.Auth__WrapperTitle}>
                <div style={{marginTop: 40}}>
                    <LogoWhite/>
                </div>
                <div className={classes.Auth__title}>Рады вас видеть!</div>
            </div>
            <div className={classes.Auth__WrapperContent}>
                <p className={classes.Auth__Title}>{"login"}</p>
                <InputMask
                    value={username}
                    className={classNames(
                        {[classes.Auth__Input]: 1},
                        {[classes.error]: username.split("_").join("").length < 12}
                    )}
                    onChange={(e) => setUsername(e.target.value)}
                    mask={
                        lang == "ru"
                        ? "+79999999999"
                        : lang == "us"
                        ? "+19999999999"
                        : "+329999999999"
                    }
                    placeholder="+7 000 00 00 000"
                />
                <div className={classes.Auth__Input_Wrapper}>
                    <input
                        className={classes.Auth__Input__Password}
                        value={password}
                        onChange={(event) => setPassword(event.target.value)}
                        placeholder={"password"}
                        type= {hidePassword ? "password": "text"}
                    />
                    {hidePassword ? (
                        <div>
                            <EyeCloseIcon onClick={() => toggleVisiblePassword()}/>
                        </div>
                    ) : (
                        <div>
                            <EyeOpenIcon onClick={() => toggleVisiblePassword()}/>
                        </div>
                    )}
                </div>
                <div className={classes.Auth__Reset}>
                    <button onClick={() => restorePass()}>restore</button>
                </div>

                {<div>{error}</div>}
                <button
                    className={classNames(
                        {[classes.Auth__Button]: 1},
                        {
                            [classes.Auth__Button_Disabled]:
                            lang == "fr" ? username.split("_").join("").length < 13 : username.split("_").join("").length < 12
                        }
                    )}
                    onClick={() =>
                        (lang == "fr" 
                            ? username.split("-").join("").length === 13 
                            : username.split("-").join("").length === 12) && onClickAuth()
                    }
                >
                    login
                </button>
                <div className={classes.Auth__noneAcc}>
                    Нет аккаунта, тогда создайте
                </div>
                <div className={classes.Auth__Regist} onClick={onClickRegist}>
                    registration
                </div>
            </div>
        </div>
    )
}

export default Auth; 


