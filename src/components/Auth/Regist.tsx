import {changeTypeModal, registUser, setEmail, setLastname, setUsername } from "../../store/actions/authActions"; 
import {useAppDispatch, useAppSelector} from "../../store/hooks/redux";
import classNames from "classnames";
import {useEffect, useState } from "react";
import InputMask from "react-input-mask";
import classes from "./Regist.module.scss";
import EyeOpenIcon from "@/svgComponents/EyeOpenIcon";
import EyeCloseIcon from "@/svgComponents/EyeCloseIcon";
import LogoWhite from "@/svgComponents/LogoWhite"

const Regist = () => {
    const  [hidePassword, setHidePassword] = useState(true);
    const  [hideRepeatPassword, setHideRepeatPassword] = useState(true);
    const  [phone, setPhone] = useState("");
    const  [password, setPassword] = useState("");
    const  [repeatPassword, setRepeatPassword] = useState("");
    const  [errorInput, setErrorInput] = useState("");
    const  [validateEmail, setValidateEmail] = useState(false);
    const  [lang, setLang] = useState("ru");

    const dispatch = useAppDispatch();
    const {registError, username, lastname, email} = useAppSelector((state) => state.auth);

    const toggleVisiblePassword = () => {
        setHidePassword(!hidePassword);
    }

    const toggleVisibleRepeatPassword = () => {
        setHideRepeatPassword(!hideRepeatPassword);
    }

    const onClickRegist = async () => {
        try
        {
            if (!username || !lastname || !email || !phone || !password)
            {
                setErrorInput("Заполните все поля");
                return;
            }
            if (password !== repeatPassword)
            {
                setErrorInput("Пароли не совпадают");
                return;
            }
            const lang = typeof window !== "undefined" && window.localStorage.getItem("lang");
            setErrorInput("");
            await dispatch(registUser({phone, password, email}, lang ? lang : "ru"));
            dispatch(changeTypeModal("code"));
        }
        catch (error)
        {
            setErrorInput("Некоторая ошибка")
        }
    }

    useEffect(() => {
        const lang = typeof window !== "undefined" && window.localStorage.getItem("lang");
        if (lang) setLang(lang);
    }, [])

    const validatorEmail = (email: any) => {
        const validate = email.match(
            /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|gov|mil|biz|info|mobi|name|aero|jobs|museum|ru)\b/
        );
        if (validate)
        {
            setValidateEmail(false);
        }
        else
        {
            setValidateEmail(true);
        }
        dispatch(setEmail(email));
    }

    return (
        <div className={classes.Regist}>
            <div className={classes.Regist_Wrapper}>
                <LogoWhite/>
            </div>
            <div className={classes.Regist__WrapperContent}>
                <div className={classes.Regist__title}>Регистрация</div>
                <input
                    className={classes.Regist__Input}
                    value={username}
                    onChange={(e) => dispatch(setUsername(e.target.value))}
                    placeholder="name"
                    type="text" 
                />
                <input
                    className={classes.Regist__Input}
                    value={lastname}
                    onChange={(e) => dispatch(setLastname(e.target.value))}
                    placeholder="lastname"
                    type="text" 
                />
                <input 
                    className={classNames(
                        {[classes.Regist__Input]: 1},
                        {[classes.error]: validateEmail}
                    )}
                    value={email}
                    onChange={(event) => validatorEmail(event.target.value)}
                    placeholder="mail"
                    type="email"
                />
                <InputMask
                    value={phone}
                    className={classNames(
                        { [classes.Regist__Input]: 1},
                        { [classes.error] : phone.split("_").join("").length < 12}
                    )}
                    onChange={(event) => setPhone(event.target.value)}
                    mask = {
                        lang == "ru" 
                            ? "+79999999999"
                            : lang == "us"
                            ? "+19999999999"
                            : "+339999999999"
                    }
                    placeholder="123 456 78 90"
                />
                <div className={classes.Regist__Input_Wrapper}>
                    <input
                        className={classNames(
                            { [classes.Regist__Input_Password] : 1},
                            { [classes.error]: !password.length}                            
                        )}
                        value={password}
                        onChange={(event) => setPassword(event.target.value)}
                        placeholder="password"
                        type={hidePassword ? "password": "text"}
                    />
                    {hidePassword ? (
                        <div>
                            <EyeCloseIcon onClick={() => toggleVisiblePassword()}/>
                        </div>
                    ) : (
                        <div>
                            <EyeOpenIcon onClick={() => toggleVisiblePassword()}/>
                        </div>
                    )}
                </div>
                <div className={classes.Regist__Input_Wrapper}>
                    <input
                        className={classNames(
                            { [classes.Regist__Input_Password] : 1},
                            { [classes.error]: !password.length}                            
                        )}
                        value={repeatPassword}
                        onChange={(event) => setRepeatPassword(event.target.value)}
                        placeholder="passwordRepeat"
                        type={hideRepeatPassword ? "password": "text"}
                    />
                    {hideRepeatPassword ? (
                        <div>
                            <EyeCloseIcon onClick={() => toggleVisibleRepeatPassword()}/>
                        </div>
                    ) : (
                        <div>
                            <EyeOpenIcon onClick={() => toggleVisibleRepeatPassword()}/>
                        </div>
                    )}
                </div>
                <div className={classes.Regist__Text}>
                    <p>*required</p>
                    <span>{errorInput}</span>
                    <span>{registError}</span>
                </div>
                <button
                    className={classNames(
                        {[classes.Regist__Button]: 1},
                        {
                            [classes.Regist__Button_Disabled]:
                            lang == "fr" 
                            ? phone.split("_").join("").length !== 13 
                            : phone.split("_").join("").length !== 12 || validateEmail    
                        }
                    )}
                    onClick={() =>
                        (lang == "fr"
                            ? phone.split("_").join("").length === 13
                            : phone.split("_").join("").length === 12) && !validateEmail && onClickRegist()
                    }
                >
                    Registration
                </button>
            </div>
        </div>
    )
}

export default Regist;