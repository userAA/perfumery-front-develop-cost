import classes from './Confirmation.module.scss';
import React, {useState} from "react";
import {useAppDispatch, useAppSelector} from "../../store/hooks/redux";
import {confirmUser} from '../../store/actions/authActions';
import Logo from '../../svgComponents/Logo';

const Confirmation = () => {
    const [code, setCode] = useState('');

    const {userId, username, confirmError, lastname} = useAppSelector((state) => state.auth);

    const dispatch = useAppDispatch();
    const onClickConfirmation = () => {
        dispatch(confirmUser(userId, code, username, lastname));
    }    

    return (
        <div className={classes.Confirmation}>
            <div className={classes.Confirmation__Wrapper}>
                <Logo/>
            </div>
            <div className={classes.Confirmation__Wrapper}>
                <p className={classes.Confirmation__Title}>Подтверждение</p>
            </div>
            <div className={classes.Confirmation__Wrapper}>
                <input
                    className={classes.Confirmation__Input}
                    value={code}
                    onChange={e => setCode(e.target.value)}
                    placeholder='Код'
                    type="text"
                />
                <div className={classes.Confirmation__Text}>
                    <p>* последние 4 цифры номера телефона, с которого поступил звонок</p>
                </div>
            </div>
            <div>{confirmError}</div>
            <div className={classes.Confirmation__Wrapper}>
                <button className={classes.Confirmation__Button} onClick={onClickConfirmation}>Подтвердить</button>
            </div>
        </div>  
    )
}

export default Confirmation;