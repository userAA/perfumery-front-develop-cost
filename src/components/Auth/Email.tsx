import classes from "./Email.module.scss"
import React, {useState} from "react"
import { userAPI } from "@/api/api";
import LogoWhite from "@/svgComponents/LogoWhite";

const Email = () => {
    const [email, setEmail] = useState("");
    const [error, setError] = useState<any>(null);

    const validatorEmail = (email:string) => {
        const validate = email.match(
            /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|gov|mil|biz|info|mobi|name|aero|jobs|museum|ru)\b/
        );
        if (!validate) return false;
        return true;
    }

    const onClickEmail = async() => {
        const validation = validatorEmail(email);
        if (!validation)
        {
            setError("Почта некорректна!");
        }
        else
        {
            try
            {
                await userAPI.restorePassword(email);
                setError("Письмо отправлено на вашу почту!");
            }
            catch (event: any)
            {
                setError(event.message);
            }
        }
    };

    return (
        <div className={classes.Email}>
            <div className={classes.Email__WrapperTitle}>
                <LogoWhite/>
                <div className={classes.Email__title}>Смена пароля.</div>
            </div>
            <div className={classes.Email__WrapperContent}>
                <p className={classes.Email__Title}>{"restoreTitle"}</p>
                <div className={classes.Email__Input_Wrapper}>
                    <input
                        className={classes.Email__Input}
                        value={email}
                        onChange={(event) => setEmail(event.target.value)}
                        placeholder="E-mail"
                        type= "text"
                    />
                </div>
                <div className={classes.Email__Message}>
                    <div>
                        {`message: ${error != null ? error: "no error"}`}
                    </div>
                </div>
                <button className={classes.Email__Button} onClick={onClickEmail}>
                    send
                </button>
            </div>
        </div>
    )
}

export default Email;