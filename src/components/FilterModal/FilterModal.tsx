"use client"
import { announcementAPI } from "@/api/api";
import InputSlider from "../../components/UI/MUI/InputSlider";
import TreeDropDown from "../../components/UI/MUI/TreeDropDown";
import {
    getAllAnnouncement,
    setFilterBottleType,
    setFilterBrand,
    setFilterCity,
    setFilterIsNew,
    setFilterName,
    setFilterPrice
} from "../../store/actions/announcementsActions";
import { useAppDispatch, useAppSelector } from "@/store/hooks/redux";
import CloseIcon from "@mui/icons-material/Close";
import { useEffect, useState } from "react";
import "../../style/globals.scss";
import ButtonFilter from "../UI/ButtonFliter";
import css from "./FilterModal.module.scss";

type PropsType = {
    closeModal: () => void;
};

const FilterModal = ({closeModal}: PropsType) => {
    const [count, setCount] = useState(0);
    const dispatch = useAppDispatch();
    const {filter} = useAppSelector((state) => state.announcements);

    const getCount = () => {
        announcementAPI.getAllAnnouncement(20, 0, filter).then((res) => {
            setCount(res.data.data.length);
        })
    }

    useEffect(() => {
        getCount();
    }, [filter])

    const setAnnouncements = () => {
        dispatch(getAllAnnouncement(20, 0, filter));
        closeModal();
    }

    const resetAnnouncements = () => {
        dispatch(setFilterPrice([0, 100000]));
        dispatch(setFilterBrand(""));
        dispatch(setFilterCity(""));
        dispatch(setFilterName(""));
        dispatch(setFilterIsNew(""));
        dispatch(setFilterBottleType(""));
    }

    return (
        <div className={css.filterModal}>
            <div className={css.filterModal__wrapper}>
                <div className={css.filterModal__topMenu}>
                    <p>filter</p>
                    <button
                        style={{cursor: "pointer", outline: "none"}}
                    >
                        <CloseIcon
                            sx={{fontSize: 40}}
                            onClick={() => {closeModal()}}
                        />
                    </button>
                </div>
                <InputSlider/>
                <TreeDropDown/>
                <ButtonFilter onClickChildren={setAnnouncements}>
                    {count === 0 ? (
                        "Ничего не найдено"
                    ) : (
                        <>
                            {"show"} {count} {"showProduct"}
                        </>
                    )}
                </ButtonFilter>
                <div style={{paddingBottom: 24}}>
                    <ButtonFilter onClickChildren={resetAnnouncements}>
                        {"resetFilter"}
                    </ButtonFilter>
                </div>
            </div>
        </div>
    )
}
export default FilterModal;

