import React from "react";
import "../../style/ui.scss";

const ButtonSecondary = ({children, className, type, listen}:
                         {children?: any; className?: any; type: any; listen?: any}) => {
    if (type) {
        return (
            <button
                type="submit"
                className={type === "primary" ? `secondaryButtonPrimary ${className}` : `secondaryButtonSecondary ${className}`}
                onClick={() => (listen ? listen() : null)}
            >
                {children}
            </button>
        )
    } else {
        return (
            <button onClick={() => (listen ? listen() : null)} className={className}>
                {children}
            </button>
        )
    }
}

export default ButtonSecondary;