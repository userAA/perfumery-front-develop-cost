import React from "react";

const InputProfile = ({title, type, pattern, value, setValue}) => {
    return (
        <div className="inputProfile">
            <p className="inputProfile__placeholder">{title}</p>
            <input
                className="inputProfile__input"
                type={type}
                pattern={pattern}
                value={value}
                onChange = {(event) => setValue(event.target.value)}
            />
        </div>
    )
}

export default InputProfile;