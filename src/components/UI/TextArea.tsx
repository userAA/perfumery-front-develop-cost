"use client";
import "../../style/ui.scss";
import {motion} from "framer-motion";
import React from "react";

const TextArea = ({title, value, setValue}) => {
    return (
        <div className="inputProfile">
            <p className="inputProfile__placeholder">{title}</p>

            <motion.textarea
                whileFocus={{scale: 1.02}}
                value={value}
                onChange={(event) => setValue(event.target.value)}
                style={{
                    width: "100%",
                    height: "180px",
                    resize: "none",
                    textAlign: "start",
                    letterSpacing: "normal"
                }}
                className="inputProfile__input"
            >
            </motion.textarea>
        </div>
    )
}

export default TextArea;