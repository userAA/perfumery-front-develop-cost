import React from "react";

const Loader = () => {
    return <><svg version="1.1" id="L9" width='40px' height='26px' xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 100" enableBackground="new 0 0 0 0" xmlSpace="preserve">
    <rect x="20" y="30" width="15" height="50" fill="#fff">
      <animateTransform attributeType="xml" attributeName="transform" type="translate" values="0 0; 0 20; 0 0" begin="0" dur="0.6s" repeatCount="indefinite"></animateTransform>
    </rect>
        <rect x="40" y="30" width="15" height="50" fill="#fff">
      <animateTransform attributeType="xml" attributeName="transform" type="translate" values="0 0; 0 20; 0 0" begin="0.2s" dur="0.6s" repeatCount="indefinite"></animateTransform>
    </rect>
        <rect x="60" y="30" width="15" height="50" fill="#fff">
      <animateTransform attributeType="xml" attributeName="transform" type="translate" values="0 0; 0 20; 0 0" begin="0.4s" dur="0.6s" repeatCount="indefinite"></animateTransform>
    </rect>
</svg></>
}

export default Loader;