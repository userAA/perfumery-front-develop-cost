import React from "react";
import "../../style/ui.scss";

const Wrapper = ({children}) => {
    return <div className="wrapper">{children}</div>
};

export default Wrapper;