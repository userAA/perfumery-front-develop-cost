import React from "react";
import {motion} from "framer-motion";

const AnimationSvg = ({children}) => {
    return <motion.div whileHover = {{scale: 1.1}}>{children}</motion.div>
};

export default AnimationSvg;