import React from "react";
import "../../style/ui.scss";

const ButtonFilter = ({children, onClickChildren}) => {
    return (
        <button onClick={onClickChildren} className="buttonFilter">
            {children}
        </button>
    )
}

export default ButtonFilter;