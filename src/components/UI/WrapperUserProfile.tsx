import React from "react";

const WrapperUserProfile = ({children}) => {
    return (
        <div style={{maxWidth: "880px", margin: "0 auto"}}>{children}</div>
    )
}

export default WrapperUserProfile;