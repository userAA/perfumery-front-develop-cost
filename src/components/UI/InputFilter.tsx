import {useEffect, useState} from 'react';
import '../../style/ui.scss';

const InputFilter = ({inputChange, defaultValue}) => {
    const [value, setValue] = useState('');

    useEffect(() => {
        if (defaultValue)
        {
            setValue(defaultValue.name);
            inputChange(defaultValue);
        }
    }, [defaultValue]);

    return (
        <input
            value={value}
            onChange={event => {setValue(event.target.value); inputChange(event.target.value)}}
            type='search'
            placeholder='search'
            className='inputFilter'
        />
    )    
}

export default InputFilter;