import React from "react";

interface IButton {
    children: React.ReactNode;
    className?: any;
    type?: string;
    [key: string]: any;
}

const Button: React.FC<IButton> = ({ children, className, type, ...props }) => {
    if (type) {
        return (
            <button
                type="submit"
                className={type === "primary" ? `mainButtonPrimary ${className}` : `mainButtonSecondary ${className}`}
                {...props}
            >
                {children}
            </button>
        );
    } 
    else 
    {
        return <button className={className}>{children}</button>;
    }
};

export default Button;