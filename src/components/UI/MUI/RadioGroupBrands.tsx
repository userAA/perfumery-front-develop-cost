import FormControl from "@mui/material/FormControl";
import FormControlLabel from "@mui/material/FormControlLabel";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import { useEffect } from "react";

export default function RadioGroupBrands({items, setItem, item}) {
    const classRadioButtonsGroup = {
        '.MuiFormControlLabel-root': {
            color: '#929192 !important',
            fontSize: '18px !important',
            fontWeight: '700 !important',
        },
        '.MuiFormControlLabel-root:focus': {
            color: '#000 !important',
            fontSize: '18px !important',
            fontWeight: '700 !important',
        },
        '.MuiRadio-colorPrimary': {
            color: '#000 !important'
        },
        '.MuiRadio-root': {
            transition: '.5s'
        },
        '.MuiRadio-root:hover': {
            backgroundColor: '#0001a !important'
        }
    }

    useEffect(() => {
        if (item) {
            setItem(item);
        }
    }, [item])

    return (
        <FormControl sx={{width: '100%'}}>
            <RadioGroup
                defaultValue='none'
                name='radio-buttons-group'
                sx={classRadioButtonsGroup}
            >
                <>
                    {items.map(item => {
                        return (
                            <FormControlLabel
                                value={item.name}
                                onChange={() => setItem(item)}
                                control={<Radio/>}
                                label={item.name}
                            />
                        )
                    })}
                </>
            </RadioGroup>
        </FormControl>
    )
}
