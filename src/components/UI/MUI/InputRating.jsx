"use client";
import "../../../style/ui.scss";
import React, {useState} from "react";
import Rating from "@mui/material/Rating";
import Box    from "@mui/material/Box";
import PerfumeFill from "@/svgComponents/PerfumeFill";
import PerfumeNonFill from "@/svgComponents/PerfumeNonFill";

const labels = {0: "0", 1: "1", 2: "2", 3: "3", 4: "4", 5: "5"};

function getLabelText(value) {
    return `${value} Star${value !== 1 ? "s" : ""}, ${labels[value]}`
}

export default function HoverRating({className, type}) {
    const [value, setValue] = useState(0);
    const [hover, setHover] = useState(-1);

    return (
        <Box
            style={{display: "flex", alignItems: "center"}}
        >
            <Rating
                name="hover-feedback"
                value={value}
                precision={1}
                getLabelText={getLabelText}

                onChange        =  {(event, newValue) => {setValue(newValue)}}
                onChangeActive  =  {(event, newHover) => {setHover(newHover)}}

                icon={
                    <PerfumeFill
                        style={{opacity: 0.55, transition: "0.5s"}}
                        fontSize="inherit"
                    />
                }

                emptyIcon={
                    <PerfumeNonFill
                        style={{opacity: 0.55, transition: "0.5s"}}
                        fontSize="inherit"
                    />
                }
            />

            {value !== null && (
                <Box sx={{ml: 2}}>
                    <p
                        style={{fontWeight: "700"}}
                        className={type === "primary" ? `ratingPrimary ${className}` : `ratingSecondary ${className}`}
                    >
                        {labels[hover !== -1 ? hover : value]}/5
                    </p>
                </Box>
            )}
        </Box>
    )    
}

