"use client";
import {adminApi, announcementAPI} from "../../../api/api";
import InputFilter from "../InputFilter";
import RadioGroupBrands from "./RadioGroupBrands";
import {IBrand, IPerfume} from "../../../models/announcements.model";
import AddIcon from "@mui/icons-material/Add";
import RemoveIcon from "@mui/icons-material/Remove";
import {TreeItem} from "@mui/x-tree-view/TreeItem";
import {TreeView} from "@mui/x-tree-view/TreeView";
import i18next from "i18next";
import {useState, useEffect} from "react";
import {useAppDispatch, useAppSelector } from "@/store/hooks/redux";
import { 
    setFilterBottleType,
    setFilterBrand, 
    setFilterCity,
    setFilterCountry,
    setFilterIsNew,
    setFilterName 
} from "@/store/actions/announcementsActions";

export default function FileSystemNavigator() {
    const dispatch = useAppDispatch();
    const {filter: {brand, name, city, bottleType, isNew, country}} = useAppSelector((state) => state.announcements)

    const [brands, setBrands] = useState<Array<IBrand>>([]);
    const [brandInputChange, setBrandInputChange] = useState<string>("");

    const getBrands = async () => {
        const res = await announcementAPI.getAllBrands(10, 0, brandInputChange);
        setBrands(res.data.data);
    }

    useEffect(() => {
        if (brandInputChange) getBrands();
        else setBrands([]);
    }, [brandInputChange]);

    const [perfumes, setPerfumes] = useState<Array<IPerfume>>([]);
    const [perfumeInputChange, setPerfumeInputChange] = useState<string>("");

    const getPerfumes = async () => {
        if (brand) {
            const res = await announcementAPI.getAllPerfumes(10, 0, brand.id, perfumeInputChange);
            setPerfumes(res.data.data);
        } else {
            const res = await announcementAPI.getAllPerfumes(10, 0, "", perfumeInputChange);
            setPerfumes(res.data.data)
        }
    }

    useEffect(() => {
        if (perfumeInputChange) getPerfumes();
        else setPerfumes([]);
    }, [perfumeInputChange]);

    const countries = ([
        {id: "france_fr", name: "france", code: "FR"},
        {id: "russia_ru", name: "russia", code: "RU"},
        {id: "usa_en", name: "usa", code: "US"}
    ]);
    const [countriesItem, setCountriesItem] = useState<any>({}); 

    const [cityInputChange, setCityInputChange] = useState<any | null>();
    const [cities, setCities]                   = useState<Array<IBrand> | null>([]);

    const getCities = async (city: string = "") => {
        const {data} = await adminApi.getCities(i18next.language, countriesItem.code, city);
        return data;
    }    

    useEffect(() => {
        (async () => {
            const data = await getCities();
            const preCities = data.map((item: any, index: number) => {
                return {id: index, name: item.name}
            });
            const cities = Array.from(new Set(preCities.map((item: any) => item.name)))
            .map((name) => preCities.find((item: any) => item.name === name))
            setCities(cities);
        })();
    }, [countriesItem])    

    useEffect(() => {
        (async () => {
            const data = await getCities(cityInputChange);
            const preCities = data.map((item: any, index: number) => {
                return {id: index, name: item.name}
            });
            const cities = Array.from(new Set(preCities.map((item: any) => item.name)))
            .map((name) => preCities.find((item: any) => item.name === name));
            setCities(cities);
        })();
    }, [cityInputChange]);

    const classTreeItem = {
        ".MuiTreeItem-label": {
            color: "#000 !important",
            fontSize: "25px !important",
            fontWeight: "700 !important",
        },
        ".MuiTreeItem-content": {
            color: "none !important",
            fontSize: "0 !important",
            fontWeight: "0 0 12px 0 !important",
        },
        ".MuiSvgIcon-root": {
            color: "#000 !important",
            fontSize: "25px !important",
            fontWeight: "700 !important",
        },
        ".MuiCollapse-root": {
            margin: "0 30px 0px 30px",
        },
    }

    return (
        <TreeView
            aria-label="file system navigator"
            defaultCollapseIcon = {<RemoveIcon/>}
            defaultExpandIcon   = {<AddIcon/>   }
            sx={{transition: "0.5s"}}
        >
            <TreeItem nodeId="1" label={"brand"} sx={classTreeItem}>
                <InputFilter inputChange={setBrandInputChange} defaultValue={brand}/>
                <RadioGroupBrands
                    items={brands}
                    setItem={(brand) => dispatch(setFilterBrand(brand))}
                    item={brand}
                />
            </TreeItem>

            <TreeItem nodeId="2" label={"nameProduct"} sx={classTreeItem}>
                <InputFilter inputChange={setPerfumeInputChange} defaultValue={name}/>
                <RadioGroupBrands
                    items={perfumes}
                    setItem={(name) => dispatch(setFilterName(name))}
                    item={name}
                />
            </TreeItem>

            <TreeItem nodeId="3" label={"format"} sx={classTreeItem}>
                <RadioGroupBrands
                    items = {[
                        {name: "bottle"},
                        {name: "sample"},
                        {name: "tester"},
                        {name: "otlivant"},
                        {name: "set"}
                    ]}
                    setItem={(bottleType) => dispatch(setFilterBottleType(bottleType))}
                    item={bottleType}
                />
            </TreeItem>

            <TreeItem nodeId="4" label="condition" sx={classTreeItem}>
                <RadioGroupBrands
                    items = {[{name: "new"}, {name: "used"}]}
                    setItem={(isNew) => dispatch(setFilterIsNew(isNew))}
                    item={isNew}
                />
            </TreeItem>

            <TreeItem nodeId="5" label="country" sx={classTreeItem}>
                <RadioGroupBrands
                    items={countries}
                    setItem={(cntr) => {
                        setCountriesItem(cntr);
                        dispatch(setFilterCountry(cntr)) 
                    }}
                    item={country}
                />
            </TreeItem>

            <TreeItem nodeId="6" label={"city"} sx={classTreeItem}>
                <InputFilter inputChange={setCityInputChange} defaultValue={city}/>
                <RadioGroupBrands
                    items={cities}
                    setItem={(city) => setFilterCity(city)}
                    item={undefined}
                />
            </TreeItem>
        </TreeView>
    )    
}