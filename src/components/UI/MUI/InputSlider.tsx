import { setFilterPrice } from "@/store/actions/announcementsActions";
import { useAppDispatch, useAppSelector } from "@/store/hooks/redux";
import Slider from '@mui/material/Slider';
import { useEffect, useState } from "react";

const MinValue = 0;
const MaxValue = 100000;

export default function InputSlider({}) {
    const dispatch = useAppDispatch();
    const {filter} = useAppSelector(state => state.announcements);
    const [value, setValue] = useState([filter.price[0], filter.price[1]]);

    const handleChange = (_ : any, newValue: any) => {setValue(newValue)};
    useEffect(() => {dispatch(setFilterPrice(value))}, [value]);

    return (
        <div style={{display: 'flex', justifyContent: 'space-between', flexWrap: 'wrap', padding: '0 10px'}}>
            <p style={{color: 'black', fontWeight: '700', fontSize: '20px'}}>
                from {filter.price[0].toLocaleString('ru')}₽
            </p>
            <p style={{color: 'black', fontWeight: '700', fontSize: '20px'}}>
                before {filter.price[1].toLocaleString('ru')}₽
            </p>
            <Slider
                style={{gridColumn: 'span 2'}}
                getAriaLabel={() => 'Temperature range'}
                value ={filter.price}
                onChange={handleChange}
                valueLabelDisplay="off"
                min={MinValue}
                max={MaxValue}
                sx={{
                    color: '#000',
                    size: '100',
                    '& .MuiSlider-thumb': {
                        borderRadius: '25px',
                        border: '2px solid #000',
                        background: '#fff',
                        '&:hover, &.Mui-focusVisible': {boxShadow: '0px 0px 0px 0px'},
                        '&.Mui-active': {boxShadow: '0px 0px 0px 0px'}
                    }
                }}
            />
        </div>
    )

}