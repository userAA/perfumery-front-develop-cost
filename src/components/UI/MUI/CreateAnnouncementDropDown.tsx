"use client";
import InputFilter from "../InputFilter";
import RadioGroupBrands from "./RadioGroupBrands";
import AddIcon from "@mui/icons-material/Add";
import RemoveIcon from "@mui/icons-material/Remove";
import { TreeItem } from "@mui/x-tree-view/TreeItem";
import { TreeView } from "@mui/x-tree-view/TreeView";

export default function FileSystemNavigator({
    brands,     setBrandInputChange, setBrandValue,   brand,
    perfumes,   setPerfumeChange,    setPerfumeValue, perfume,
    formats,    setFormat,                            format,
    stateItems, setState,                             state,
                setPrice,                             price,
                setVolume,                            volume,
    countries,  setCountry,                           country,
    cities,     setCity, city, setInputCity, inputCity
}) {
    const classTreeItem = {
        ".MuiTreeItem-label": {
            color: "#000 !important",
            fontSize: "25px !important",
            fontWeight: "700 !important"
        },
        ".MuiTreeItem-content": {
            background: "node !important",   
            padding: "0 !important",    
            margin: "0 0 12px 0 !important"
        },
        ".MuiSvgIcon-root": {
            color: "#000 !important",
            fontSize: "25px !important",
            fontWeight: "700 !important",
        },
        ".MuiCollapse-root": {
            marginLeft: "30px",
            marginRight: "30px",
            margin: "0 30px 0px 30px",
        }
    }

    return (
        <TreeView
            aria-label="file system navigator"
            defaultCollapseIcon = {<RemoveIcon/>}
            defaultExpandIcon   = {<AddIcon/>}
            sx = {{transition: "0.5s"}}
            multiSelect
        >
            <TreeItem nodeId="1" label={"brand"} sx={classTreeItem}>
                <InputFilter inputChange={setBrandInputChange} defaultValue={brand}/>
                <RadioGroupBrands items={brands} setItem={setBrandValue} item={brand}/>
            </TreeItem>

            <TreeItem nodeId="2" label={"nameProduct"} sx={classTreeItem}>
                <InputFilter inputChange={setPerfumeChange} defaultValue={perfume}/>
                <RadioGroupBrands items={perfumes} setItem={setPerfumeValue} item={perfume}/>
            </TreeItem>

            <TreeItem nodeId="3" label={"format"} sx={classTreeItem}>
                <RadioGroupBrands items={formats} setItem={setFormat} item={format}/>
            </TreeItem>

            <TreeItem nodeId="4" label={"condition"} sx={classTreeItem}>
                <RadioGroupBrands items={stateItems} setItem={setState} item={state}/>
            </TreeItem>

            <TreeItem nodeId="6" label={"price"} sx={classTreeItem}>
                <input
                    style={{
                        color: "#000",
                        outline: "none",
                        backgroundColor: " 10px",
                        borderRadius: " 10px",
                        border: "2px solid #000",
                        padding: " 10px",
                        textAlign: "center",
                        letterSpacing: " 10px",
                        fontWeight: "700",
                        fontSize: "20px"
                    }}
                    value={price}
                    onChange={(event) => setPrice(event.target.value)}
                    type="text"
                />
            </TreeItem>  

            <TreeItem nodeId="7" label={"volume"} sx={classTreeItem}>
                <input
                    style={{
                        color: "#000",
                        outline: "none",
                        backgroundColor: "#FFF",
                        borderRadius: " 10px",
                        border: "2px solid #000",
                        padding: " 10px",
                        textAlign: "center",
                        letterSpacing: " 10px",
                        fontWeight: "700",
                        fontSize: "20px"
                    }}
                    value={volume}
                    onChange={(event) => setVolume(event.target.value)}
                    type="text"
                />
            </TreeItem>          

            <TreeItem nodeId="8" label={"country"} sx={classTreeItem}>
                <RadioGroupBrands items={countries} setItem={setCountry} item={country} />
            </TreeItem>

            <TreeItem nodeId="9" label={"city"} sx={classTreeItem}>
                <InputFilter inputChange={setInputCity} defaultValue={inputCity}/>
                <RadioGroupBrands items={cities} setItem={setCity} item={city}/>
            </TreeItem> 
        </TreeView>
    )
}






