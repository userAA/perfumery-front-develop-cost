"use client";
import { setSorted } from "@/store/actions/announcementsActions";
import { useAppDispatch, useAppSelector } from "@/store/hooks/redux";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import {AnimatePresence, motion} from "framer-motion";
import {useState} from "react";
import css from "./FilterCatalogList.module.scss";

const FilterCatalogList = ({children}) => {
    const [isOpen, setIsOpen] = useState(false);
    const [selectItem, setSelectItem] = useState(1);

    const dispatch = useAppDispatch();
    const {announcementsItems} = useAppSelector((state) => state.announcements);

    return (
        <motion.nav initial={false} className="menu">
            <motion.button
                style={{
                    display: "flex",
                    alignItems: "center",
                    fontSize: "16px",
                    cursor: "pointer",
                    fontWeight: "700",
                    color: "#000",
                    background: "none",
                    width: "100%",
                    transform: "scale(1) translateZ(0) !important",
                    outline: "none"
                }}
                whileTap={{scale: 0.85}}
                whileHover={{scale: 0.95}}
                onClick={() => setIsOpen(!isOpen)}
            >
                {children}
                <motion.div
                    style={{display: "flex", alignItems: "center"}}
                    variants={{
                        open: {transform: "rotate(180deg)"},
                        closed: {transform: "rotate(0deg"}
                    }}
                >
                    <KeyboardArrowDownIcon/>
                </motion.div>
            </motion.button>
            <AnimatePresence>
                {isOpen && (
                    <motion.ul
                        initial={{opacity: 0, height: 0}}
                        animate={{opacity: 1, height: "auto"}}
                        exit={{opacity: 0, height: 0}}
                        transition={{duration: 0.2}}
                        style={{
                            backgroundColor: "#f2f1f0",
                            position: "absolute",
                            padding: "10px 30px 20px 30px",
                            boxShadow: "0 0 2px rgba(0, 0, 0, 0.36)",
                            border: "1px solid rgba(0,0,0,0.3)",
                            width: "auto",
                            margin: "10px 0px 0px -20px",
                            zIndex: "2",
                            pointerEvents: "auto",
                            cursor: "pointer",
                            overflow: "hidden",
                            borderRadius: 10
                        }}
                    >
                        <motion.li
                            onClick={() => {
                                setSelectItem(1);
                                dispatch(setSorted("decreasing", announcementsItems));
                            }}
                            className={selectItem === 1 ? css.llItem__Active : css.liItem}
                            whileHover={{scale: 0.95}}
                            whileTap={{scale: 0.9}}
                        >
                            {"cheaper"}
                        </motion.li>   
                        <motion.li
                            onClick={() => {
                                setSelectItem(2);
                                dispatch(setSorted("increase", announcementsItems));
                            }}
                            className={selectItem === 2 ? css.llItem__Active : css.liItem}
                            whileHover={{scale: 0.95}}
                            whileTap={{scale: 0.9}}
                        >
                            {"expensive"}
                        </motion.li>    
                        <motion.li
                            onClick={() => {
                                setSelectItem(3);
                                dispatch(setSorted("date", announcementsItems));
                            }}
                            className={selectItem === 3 ? css.llItem__Active : css.liItem}
                            whileHover={{scale: 0.95}}
                            whileTap={{scale: 0.9}}
                        >
                            {"byDate"}
                        </motion.li> 
                        <motion.li
                            onClick={() => {
                                setSelectItem(4);
                                dispatch(setSorted("dateDown", announcementsItems));
                            }}
                            className={selectItem === 4 ? css.llItem__Active : css.liItem}
                            whileHover={{scale: 0.95}}
                            whileTap={{scale: 0.9}}
                        >
                            {"byDateDown"}
                        </motion.li>    
                    </motion.ul>
                )}
            </AnimatePresence>
        </motion.nav>
    )
}

export default FilterCatalogList;