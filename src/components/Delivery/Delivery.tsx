import "../../style/globals.scss";
import "../../style/ui.scss";
import React from "react";
import css from "./Delivery.module.scss";
import InputProfile from "../UI/InputProfile";
import WrapperUserProfile from "../UI/WrapperUserProfile";

const Delivery = ({city, setCity, address, setAddress}) => {
    return (
        <WrapperUserProfile>
            <div className={css.delivery}>
                <h2>{"delivery"}</h2>
                <div className={css.delivery__topMenu}>
                    <InputProfile
                        title={"country"}
                        type={"text"}
                        value={address.country}
                        setValue={(value) => setAddress({...address, country: value})}
                    />
                    <InputProfile
                        title={"city"}
                        type={"text"}
                        value={city}
                        setValue={setCity}
                    />
                    <InputProfile
                        title={"index"}
                        type={"text"}
                        value={address.index}
                        setValue={(value) => setAddress({...address, index: value})}
                    />
                </div>
                <div className={css.delivery__bottomMenu}>
                    <InputProfile
                        title={"street"}
                        type={"text"}
                        value={address.street}
                        setValue={(value) => setAddress({...address, street: value})}
                    />     
                    <InputProfile
                        title={"house"}
                        type={"text"}
                        value={address.house}
                        setValue={(value) => setAddress({...address, house: value})}
                    /> 
                    <InputProfile
                        title={"apartment"}
                        type={"text"}
                        value={address.apartment}
                        setValue={(value) => setAddress({...address, apartment: value})}
                    /> 
                </div>
            </div>
        </WrapperUserProfile>
    )
};

export default Delivery;