"use client"
import "../../style/globals.scss";
import { useEffect } from "react";
import styles from "./ProfileSection.module.scss";
import { useAppDispatch, useAppSelector } from "@/store/hooks/redux";
import Link from "next/link";
import classNames from "classnames";
import ButtonSecondary from "../UI/ButtonSecondary";
import MessageSvg from "@/svgComponents/MessageSvg";
import FavouritesSvg from "@/svgComponents/FavoriteSvg";
import SettingMenuSvg from "@/svgComponents/SettingMenuSvg";
import { getProfile } from "@/store/actions/profileActions";
import InputRating from "../../components/UI/MUI/InputRating";
import {motion} from "framer-motion";
import AvatarIcon from "@/svgComponents/AvatarIcon";
import PerfumeIcon from "@/svgComponents/PerfumeIcon";

const ProfileSection = () => {
    const dispatch = useAppDispatch();

    const {user} = useAppSelector((state) => state.profile);

    useEffect(() => {
        dispatch(getProfile());
    }, [])

    return (
        <section className={styles.profile}>
            <div className="container">
                <div className={styles.profile__container}>
                    <div className={styles.header}>
                        <div className={classNames(styles.view, styles.wrapper)}>
                            <div className={styles.photo}>
                                <AvatarIcon/>
                                <h2>
                                    {user?.firstname} {user?.lastname}
                                </h2>
                                <h3>
                                    {user?.city ? "cityUser" + user?.city : "noCity"}
                                </h3>
                            </div>
                            <div className={styles.link}>
                                <motion.div
                                    initial={{scale: 1}}
                                    whileHover={{scale: 0.98}}
                                    whileTap={{scale: 0.95}}
                                >
                                    <Link
                                        href={`/messages`}
                                        style={{width: "80px", height: "80px"}}
                                    >
                                        <ButtonSecondary type={"primary"}>
                                            <MessageSvg color={"#fff"}/>
                                        </ButtonSecondary>
                                    </Link>
                                </motion.div>

                                <motion.div
                                    initial={{scale: 1}}
                                    whileHover={{scale: 0.98}}
                                    whileTap={{scale: 0.95}}
                                >
                                    <Link href={`#`} style={{width: "80px", height: "80px"}}>
                                        <ButtonSecondary type={"primary"}>
                                            <FavouritesSvg color={"#fff"}/>
                                        </ButtonSecondary>
                                    </Link>
                                </motion.div>

                                <motion.div
                                    initial={{scale: 1}}
                                    whileHover={{scale: 0.95}}
                                    whileTap={{scale: 0.95}}
                                >
                                    <Link
                                        href={`/profile/settings`}
                                        style={{width: "80px", height: "80px"}}
                                    >
                                        <ButtonSecondary type={"primary"}>
                                            <SettingMenuSvg/>
                                        </ButtonSecondary>
                                    </Link>
                                </motion.div>
                            </div>
                        </div>

                        <div className={classNames(styles.announcement, styles.wrapper)}>
                            <PerfumeIcon/>
                            <div className={styles.announcement__wrapper}>
                                <h2 className={styles.announcement__title}>{"exposer"}</h2>
                                <h2 className={styles.announcement__text}>{"exposerText"}</h2>
                            </div>
                            <Link
                                href={`/profile/create/`}
                                className={styles.announcement__button}
                            >
                                {"create"}
                            </Link>
                         </div>

                        <div className={classNames(styles.info, styles.wrapper)}>
                            <h3 className={styles.info__title}>{"info"}</h3>
                            <div className={styles.rating}>
                                <div>{"rating"}</div>
                                <InputRating type={"primary"}/>
                            </div>
                            <div className={styles.reviews}>
                                <div className={styles.reviews__line}></div>
                                <Link
                                    className={styles.reviews__link}
                                    href={`/profile/rating/${user.id}`}
                                >
                                    {"reviews"}
                                </Link>
                            </div>

                            <div className={styles.stat}>
                                <div className={styles.stat__item}>
                                    <p className={styles.stat__item_name}>{"sales"}</p>
                                    <div className={styles.stat__item_value}>0</div>
                                </div>
                                <div className={styles.stat__item}>
                                    <p className={styles.stat__item_name}>{"purchases"}</p>
                                    <div className={styles.stat__item_value}>0</div>
                                </div>                               
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    )
}

export default ProfileSection;