"use client"

import React, {useEffect, useState} from "react"
import Image from "next/image"
import css from "./Baner.module.scss"
import {motion} from "framer-motion"
import classNames from "classnames"

const Baner = () => {
    const [windowWidth, setWindowWidth] = useState(1025);

    useEffect(() => {
        setWindowWidth(typeof window !== "undefined" ? window.innerWidth : 1024);
    }, [])

    return (
        <motion.div initial={{opacity: 0.7}} animate={{opacity: 1}}>
            {windowWidth > 1024 ? 
                (
                    <div className={css.baner}>
                        <div className={css.baner__first}>
                            <h2 className={css.baner__title}>
                                Открой новую онлайн площадку с огромным выбором парфюмерии
                            </h2>
                            <h3 className={css.baner__subtitle}>
                                BYSmell представляет возможность покупать и продавать новую и б/у
                                парфюмерию среди всех пользоватеей платформы
                            </h3>
                            <button className={css.baner__button}>Смотреть</button>
                            <Image
                                src="/RedPhoto.png"
                                alt="red"
                                width={584}
                                height={730}
                                className={css.baner__chanel}
                            />
                        </div>
                        <div className={css.baner__second}>
                            <Image
                                src="/ChanelPhoto.png"
                                alt="chanel"
                                width={620}
                                height={620}
                                className={css.baner__red}
                            />
                            <h2 className={classNames(css.baner__title, css.baner__rightTitle)}>
                                Лучшие предложения от продавцев из России, Франции и Англии
                            </h2>
                            <button
                                 className={classNames(css.baner__button, css.baner__buttonNone)}
                            >
                                Смотреть
                            </button>
                        </div>
                    </div>    
                ) : (
                    <>
                        {windowWidth < 768 ? (
                            <div className={css.baner}>
                                <Image
                                    src="/ChannelPhoto.png"
                                    alt="chanel"
                                    width={620}
                                    height={560}
                                    className={css.baner__red}
                                />
                                <h2 className={css.baner__title}>
                                    Открой новую онлайн площадку с огромным выбором парфюмерии
                                </h2>
                                <h3 className={css.baner__subtitle}>
                                    BYSmell представляет возможность покупать и продавать новую и
                                    б/у парфюмерию среди всех пользоватеей платформы
                                </h3>
                                <Image
                                    src="/RedPhoto.png"
                                    alt="red"
                                    width={584}
                                    height={730}
                                    className={css.baner__chanel}
                                />
                                <h2
                                    className={classNames(css.baner__title, css.baner__rightTitle)}
                                >
                                    Лучшие предложения от продавцев из России, Франции и Англии
                                </h2>
                                <button className={classNames(css.baner__button, css.buttonCenter)}>
                                    Смотреть
                                </button>
                            </div>
                        ) : (
                            <div className={css.baner__container}>
                                <div className={css.baner}>
                                    <div className={css.baner__first}>
                                        <h2 className={css.baner__title}>
                                            Открой новую онлайн площадку с огромным выбором парфюмерии
                                        </h2>
                                        <h3 className={css.baner__subtitle}>
                                            BYSmell представляет возможность покупать и продавать новую
                                            и б/у парфюмерию среди всех пользоватеей платформы
                                        </h3>
                                        <Image
                                            src="/RedPhoto.png"
                                            alt="red"
                                            width={584}
                                            height={730}
                                            className={css.baner__chanel} 
                                        />
                                    </div>
                                    <div className={css.baner__second}>
                                        <Image
                                            src="/ChannelPhoto.png"
                                            alt="chanel"
                                            width={620}
                                            height={560}
                                            className={css.baner__red}
                                        />
                                        <h2
                                            className={classNames(css.baner__title, css.baner__rightTitle)}
                                        >
                                            Лучшие предложения от продавцев из России, Франции и Англии
                                        </h2>
                                    </div>
                                </div>
                                <button
                                    className={classNames(css.baner__button, css.baner__buttonCenter)}
                                >
                                    Смотреть
                                </button>
                            </div>
                        )}
                    </>
                )
            }
        </motion.div>
    )
}

export default Baner;