import React from "react";
import {useAppSelector} from "../store/hooks/redux";
import Wrapper from "@/components/UI/Wrapper";
import ProfileSection from "@/components/ProfileSection/ProfileSection";
import ProfileCatalogList from "@/components/ProfileCatalogList/ProfileCatalogList";

const ProfileHOK = ({id}) => {
    const {userId, isAuth} = useAppSelector((state) => state.auth);
    if (isAuth === null) return;

    return (
        <Wrapper>
            <ProfileSection/>
            <ProfileCatalogList children={"myPurchases"}/>
            <ProfileCatalogList children={"Активные"} userId={userId}/>
            <ProfileCatalogList children={"Закрытые"} userId={userId}/>
            <ProfileCatalogList children={"archiveSales"} />
            <ProfileCatalogList children={"archivePay"} />
            <ProfileCatalogList children={"favorites"} />
        </Wrapper>
    )
}

export default ProfileHOK;