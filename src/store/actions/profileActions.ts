import { AppDispatch } from "../store";
import { Dispatch } from "@reduxjs/toolkit";
import { IUserModelResponse } from "@/models/user.model";
import { userAPI } from "@/api/api";
import { profileSlice } from "../slices/profileSlice";

export const getProfile = (): AppDispatch => {
    return async (dispatch: Dispatch) => {
        try
        {
            dispatch(profileSlice.actions.getProfile());
            const user:IUserModelResponse = await userAPI.getUser();
            dispatch(profileSlice.actions.getProfileSuccess(user.data));
        }
        catch (e)
        {
            dispatch(profileSlice.actions.getProfileError(e));
        }
    }
}