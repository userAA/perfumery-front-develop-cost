"use client"

import { Dispatch } from "@reduxjs/toolkit"
import { userAPI} from "../../api/api";
import { AppDispatch } from "../store"
import { authSlice } from "../slices/authSlice"
import { ITokenResponse } from "../../models/auth.model";
import { TStepModal } from "../../models/auth.model"

export const userAuth = (login: string, password: string): AppDispatch => {
    return async (dispatch: Dispatch) => {
        try 
        {
            dispatch(authSlice.actions.authLoad());
            const res: ITokenResponse = await userAPI.authUser(login, password);
            if (typeof window != undefined) {
                localStorage.setItem("token", res.data.bearerToken);
                localStorage.setItem("refreshToken", res.data.refreshToken);
            }
            const response = await userAPI.getUser();
            if (response.data.isBlocked)
            {
                dispatch(authSlice.actions.authError({response: {data: {message: "blockedMessage"}}}));
            }
            else
            {
                dispatch(authSlice.actions.setRegisterUserId(response.data.id));
                dispatch(authSlice.actions.authSuccess());
                dispatch(authSlice.actions.closeModal());
            }
        }
        catch(event)
        {
            dispatch(authSlice.actions.authError(event));
        }
    }
}

export const userLogout = (): AppDispatch => {
    return async (dispatch: Dispatch) => {
        try 
        {
            dispatch(authSlice.actions.authLoad());
            if (typeof window != undefined)
            {
                localStorage.removeItem("token");
                localStorage.removeItem("refreshToken");
            }
            dispatch(authSlice.actions.logOut());
            location.replace(location.origin);
        }
        catch (event)
        {
            dispatch(authSlice.actions.authError(event));
        }
    }
}

export const openModal = (): AppDispatch => {
    return (dispatch: Dispatch) => {
        try {
            dispatch(authSlice.actions.openModal())
        } catch(e) {
            dispatch(authSlice.actions.authError(e));
        }
    }
}

export const changeTypeModal = (type: TStepModal): AppDispatch => {
    return (dispatch: Dispatch) => {
        dispatch(authSlice.actions.changeTypeModal(type))
    }
}

export const setUsername = (username: any): AppDispatch => {
    return (dispatch: Dispatch) => {
        dispatch(authSlice.actions.setUsername(username))
    }
}

export const setLastname = (lastname): AppDispatch => {
    return (dispatch: Dispatch) => {
        dispatch(authSlice.actions.setLastname(lastname))
    }
}

export const setEmail = (email): AppDispatch => {
    return (dispatch: Dispatch) => {
        dispatch(authSlice.actions.setEmail(email))
    }
}

export const registUser = (user: any, country: string): AppDispatch => {
    return async (dispatch: Dispatch) => {
        try
        {
            dispatch(authSlice.actions.authLoad());
            const response = await userAPI.registUser(user, country);

            if (country !== "ru")
            {
                localStorage.setItem("token", response.data.data.bearerToken);
                localStorage.setItem("refreshToken", response.data.data.bearerToken);

                dispatch(authSlice.actions.authLoad());
                const res = await userAPI.getUser();
                dispatch(authSlice.actions.setRegisterUserId(res.data.id));
                dispatch(authSlice.actions.authSuccess());
                dispatch(authSlice.actions.closeModal());
            }
            else
            {
                dispatch(authSlice.actions.setRegisterUserId(response.data.data.userId));
            }   
        }
        catch (e)
        {
            dispatch(authSlice.actions.authRegistError(e))    
        }
    }
}

export const confirmUser = (userId, code, username, lastname): AppDispatch => {
    return async (dispatch: Dispatch) => {
        try
        {
            dispatch(authSlice.actions.authLoad());
            const response: any = await userAPI.confirmUser(userId, code);
            if (typeof window != undefined) 
            {
                localStorage.setItem("token", response.data.data.bearerToken);
                localStorage.setItem("refreshToken", response.data.data.refreshToken);
            }
            await userAPI.getUser();
            const updateUser = await userAPI.updateUser({firstUser: username, lastname});
            dispatch(authSlice.actions.setRegisterUserId(updateUser.data.id));
            dispatch(authSlice.actions.authSuccess());
            dispatch(authSlice.actions.closeModal());
        }
        catch (event)
        {
            dispatch(authSlice.actions.authConfirmError(event))
        }
    }
}

export const closeModal = (): AppDispatch => {
    return (dispatch: Dispatch) => {
        try {
            dispatch(authSlice.actions.closeModal());
        } catch (e) {
            dispatch(authSlice.actions.authError(e));
        }
    }
}





