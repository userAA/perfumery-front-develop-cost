import { announcementAPI } from "@/api/api";
import {IBrand, IPerfume, IFilter, IAnnouncement, IAnnouncementsResponse, IAnnouncementResponse} from "../../models/announcements.model"
import {announcementsSlice} from "../slices/announcementsSlice";
import { AppDispatch } from "../store";
 
export const getAllAnnouncement = (limit: number, offset: number, filter?: IFilter): AppDispatch => {
    return async (dispatch: AppDispatch) => {
        try {
            dispatch(announcementsSlice.actions.getAnnouncementLoad());
            const res: IAnnouncementsResponse = await announcementAPI.getAllAnnouncement(limit, offset, filter);
            
            dispatch(announcementsSlice.actions.getAllSuccess({data: [...res.data?.data], count: res.data?.count}));
        } catch (e) {
            dispatch(announcementsSlice.actions.getAnnouncementError(e as Error));
        }
    }
}

export const setSorted = (viewSort: string, announcements: Array<IAnnouncement>): AppDispatch => {
    return (dispatch: AppDispatch) => {
        try
        {
            if (viewSort === "decreasing")
            {
                const newDataSort = [...announcements].sort((a,b) => a.price - b.price);
                dispatch(announcementsSlice.actions.getAllSuccess({data: newDataSort}));
            }
            else if (viewSort === "increase")
            {
                const newDataSort = [...announcements].sort((a,b) => b.price - a.price);
                dispatch(announcementsSlice.actions.getAllSuccess({data: newDataSort}));
            }
            else if (viewSort === "date")
            {
                const newDataSort = [...announcements].sort((a, b) => {
                    return new Date(b.created).getTime() - new Date(a.created).getTime()
                });
                dispatch(announcementsSlice.actions.getAllSuccess({data: newDataSort}));
            }
            else if (viewSort === "dateDown")
            {
                const newDataSort = [...announcements].sort((a, b) => {
                    return new Date(a.created).getTime() - new Date(b.created).getTime()
                });
                dispatch(announcementsSlice.actions.getAllSuccess({data: newDataSort}));
            }
        } 
        catch(e)
        {
            console.log(e)
        }
    }
}

export const getAnnouncementsById = (id: string): AppDispatch => {
    return async (dispatch: AppDispatch) => {
        try
        {
            dispatch(announcementsSlice.actions.getAnnouncementLoad());
            const res: IAnnouncementResponse = await announcementAPI.getAnnouncement(id);
            dispatch(announcementsSlice.actions.getByIdSuccess(res.data.data));
        }
        catch(e)
        {
            dispatch(announcementsSlice.actions.getAnnouncementError(e as Error));
        }
    }
}

export const setShowFilter = (show: boolean): AppDispatch => {
    return (dispatch: AppDispatch) => dispatch(announcementsSlice.actions.setShowFilter(show));
}

export const setFilterName = (name: IPerfume): AppDispatch => {
    return (dispatch: AppDispatch) => dispatch(announcementsSlice.actions.setFilterName(name));
}

export const setFilterCity = (city: string): AppDispatch => {
    return (dispatch: AppDispatch) => dispatch(announcementsSlice.actions.setFilterCity(city));
}

export const setFilterCountry = (country: string): AppDispatch => {
    return (dispatch: AppDispatch) => dispatch(announcementsSlice.actions.setFilterCountry(country));
}

export const setFilterBrand = (brand: IBrand): AppDispatch => {
    return (dispatch: AppDispatch) => dispatch(announcementsSlice.actions.setFilterBrand(brand));
}

export const setFilterPrice = (price: number[]): AppDispatch => {
    return (dispatch: AppDispatch) => dispatch(announcementsSlice.actions.setFilterPrice(price));
}

export const setFilterIsNew = (isNew: {name: string}): AppDispatch => {
    const state = isNew.name === "Новое" ? "true" : "false";
    return (dispatch: AppDispatch) => dispatch(announcementsSlice.actions.setFilterIsNew(state));
}

export const setFilterBottleType = (bottleType: string): AppDispatch => {
    return (dispatch: AppDispatch) => dispatch(announcementsSlice.actions.setFilterBottleType(bottleType));
}



