import {IProfileModel, IUserModel} from '../../models/user.model';
import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import { AxiosError } from 'axios';

const initState: IProfileModel = {
    loadingUser: false,
    errorUser: "",
    user: {
        city: "",
        created: "",
        email: "",
        firstname: "",
        id: "",
        lastname: "",
        phone: "",
        address: {
            index: "",
            street: "",
            house: "",
            apartment: ""
        }
    },
    wishList: [],
    reviews: []
}

export const profileSlice = createSlice({
    name: "profile",
    initialState: initState,
    reducers: {
        getProfile(state) {
            state.loadingUser = true;
        },
        getProfileSuccess(state, action: PayloadAction<IUserModel>) {
            state.loadingUser = false;
            state.user = action.payload;
        },
        getProfileError(state, action: PayloadAction<any>) {
            state.loadingUser = false;
            state.errorUser   = action.payload.message;
        }
    }
})

export default profileSlice.reducer;