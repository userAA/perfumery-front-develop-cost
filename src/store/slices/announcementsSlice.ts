import { IAnnouncements, IBrand, IPerfume, IAnnouncement } from '../../models/announcements.model';
import { PayloadAction, createSlice} from '@reduxjs/toolkit';

const initState: IAnnouncements = {
    loading: false,
    error: '',
    announcementsItems: [],
    count: 0,
    announcementById: {},
    showFilter: false,
    filter: {
        price: [0, 100000],
        city: '',
        brand: '',
        name: '',
        isNew: '',
        bottleType: '',
        country: ''
    }
}

export const announcementsSlice = createSlice({
    name: 'announcement',
    initialState: initState,
    reducers: {
        getAnnouncementLoad(state) {
            state.loading = true;
        },
        setShowFilter(state, action) {
            state.showFilter = action.payload
        },
        getAllSuccess(state, action: PayloadAction<{data: Array<IAnnouncement>; count?: number}>)
        {
            state.loading = false;
            state.announcementsItems = action.payload.data;
            if (action.payload.count >= 0) state.count = action.payload.count;
        },
        getAnnouncementError(state, action: PayloadAction<Error>) {
            state.loading = false;
            state.error = action.payload.message;
        },
        getByIdSuccess(state, action: PayloadAction<IAnnouncement>) {
            state.announcementById = action.payload;
            state.loading = false;
        },
        setFilterName(state, action: PayloadAction<IPerfume>) {
            state.filter.name = action.payload
        },
        setFilterBrand(state, action: PayloadAction<IBrand>) {
            state.filter.brand = action.payload
        },
        setFilterCity(state, action: PayloadAction<string>) {
            state.filter.city = action.payload
        },
        setFilterCountry(state, action: PayloadAction<string>) {
            state.filter.country = action.payload
        },
        setFilterPrice(state, action: PayloadAction<number[]>) {
            state.filter.price = action.payload
        },
        setFilterIsNew(state, action: PayloadAction<string>) {
            state.filter.isNew = action.payload
        },
        setFilterBottleType(state, action: PayloadAction<string>) {
            state.filter.bottleType = action.payload
        }
    }
})

export default announcementsSlice.reducer;