import {PayloadAction, createSlice} from '@reduxjs/toolkit'
import { IAuthModel, TStepModal } from '../../models/auth.model';
import { AxiosError } from 'axios';

const initState: IAuthModel = {
    loading: false,
    isAuth: null,
    error: "",
    stepModal: 'auth',
    modalIsOpen: false,
    userId: "",
    username: "",
    lastname: "",
    email: "",
    registError: "",
    confirmError: ""
}

export const authSlice = createSlice({
    name: "auth",
    initialState: initState,
    reducers: {
        logOut(state) {
            state = initState;
        },
        authLoad(state){
            state.loading = true;
        },
        openModal(state) {
            state.loading = false;
            state.stepModal = "auth";
            state.modalIsOpen = true;
            state.error = "";
        },
        changeTypeModal(state, action: PayloadAction<TStepModal>){
            state.stepModal = action.payload;
        },
        closeModal(state) {
            state.loading = false;
            state.stepModal  = "auth";
            state.modalIsOpen = false;
        },
        authSuccess(state) {
            state.loading = false;
            state.isAuth  = true;
        },
        setRegisterUserId(state, action: PayloadAction<string>) {
            state.userId = action.payload;
        },
        authError(state, action: PayloadAction<AxiosError>) {
            state.loading = false;
            state.error   = action.payload.response.data.message;
            state.isAuth  = false;
        },
        setUsername(state, action: PayloadAction<string>) {
            state.username = action.payload;
        },
        setLastname(state, action: PayloadAction<string>) {
            state.lastname = action.payload;
        },
        setEmail(state, action: PayloadAction<string>) {
            state.email = action.payload;
        },
        authRegistError(state, action: PayloadAction<AxiosError>)
        {
            state.loading = false;
            state.registError = action.payload.response?.data.message ===
            "An internal server error occurred" ? "Неверный номер телефона или электронная почта" : action.payload.response?.data.message;
            state.isAuth = false;
        },
        authConfirmError(state, action: PayloadAction<AxiosError>) {
            state.loading      = false;
            state.confirmError = action.payload.data.message;
            state.isAuth       = false;
        }
    }
})

export default authSlice.reducer;




