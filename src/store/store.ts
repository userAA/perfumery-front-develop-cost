import {configureStore} from "@reduxjs/toolkit";
import authReducer from "./slices/authSlice";
import announcementsReducer from "./slices/announcementsSlice"
import profileReducer from "./slices/profileSlice"

export const store = configureStore({
    reducer: {
        auth: authReducer,
        announcements: announcementsReducer,
        profile: profileReducer
    },
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware({
            serializableCheck: false
        })
})

export type RootState   = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch;