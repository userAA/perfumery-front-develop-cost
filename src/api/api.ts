"use client"

import { IFilter } from "@/models/announcements.model";
import { IRegistResponse, ITokenResponse } from "@/models/auth.model";
import { IUserModelResponse } from "@/models/user.model";
import axios from "axios";

const instance = axios.create({
    baseURL: `https://bysmell.com/api/v1`
})

export const adminApi = {
    getUsers() {
        return instance.get("/users/all", {
            headers: {
                Authorization: "Bearer " + localStorage.getItem("token")
            }
        })
    },
    getCities(lang: string, country: string, startWith: string) {
        return instance.get(`/cities?lang=${lang}&country=${country}&startWith=${startWith}`)
    },
    userBlock(id: string) {
        return instance.patch("/admin/users/block/" + id, null, {
            headers: {
                Authorization: "Bearer " + localStorage.getItem("token")
            }            
        })
    },
    userUnBlock(id: string) {
        return instance.patch("/admin/users/unblock/" + id, null, {
            headers: {
                Authorization: "Bearer " + localStorage.getItem("token")
            }            
        })
    }
}

export const userAPI = {
    authUser(login: string, password: string): Promise<ITokenResponse>{
        return instance.post("users/login", {login, password});
    },

    restorePassword(email: string): Promise<any>{
        return instance.post("users/restore", {email});
    },

    getUser(id: string = ""): Promise<IUserModelResponse> {
        const userId = id ? "/" + id : "";
        return instance.get("users" + userId, {
            headers: {
                Authorization: "Bearer " + localStorage.getItem("token")
            }
        })
    },
    registUser(user: any, country: string): Promise<IRegistResponse | any> {
        return instance.post("users", {
            email: user.email,
            phone: user.phone,
            password: user.password,
            country
        })
    },
    confirmUser(userId: string, code: string): Promise<ITokenResponse> {
        return instance.post("users/confirm", {
            userId,
            code
        })
    },
    updateUser(user: any) {
        return instance.patch("users", user, {
            headers: {
                Authorization: "Bearer " + localStorage.getItem("token")
            }
        });
    }
}

export const announcementAPI = {
    async getAnnouncement(id: number | string) {
        return await instance.get(`announcement?id=${id}`);
    },

    async getAllAnnouncement(limit: number, offset: number, filter: IFilter){
        let url = `announcement/all?limit=${limit}&offset=${offset}&moreThen=${filter.price[0]}&lessThen=${filter.price[1]}`;
        if (filter)
        {
            if (filter.name) url = url + `&name=${filter.name.name}`;
            if (filter.brand) url = url + `&brand=${filter.brand.name}`;
            if (filter.city) url = url + `&city=${filter.city}`;
            if (filter.isNew) url = url + `&isNew=${filter.isNew}`;
            if (filter.bottleType) url = url + `&bottleType=${filter.bottleType.name}`;
        }
        return await instance.get(url);
    },

    async getAllBrands(limit: number, offset: number, name: string = "") {
        return await instance.get(`brand/all?limit=${limit}&offset=${offset}&name=${name}`);
    },

    async getAllPerfumes(limit: number, offset: number, brandId: string, name: string)
    {
        let url = `perfume/all?limit=${limit}&offset=${offset}&name=${name}`;
        if (brandId) url = `perfume/all?limit=${limit}&offset=${offset}&brandId=${brandId}&name=${name}`;
        return await instance.get(url, 
        {
            headers: {Authorization: "Bearer " + localStorage.getItem("token")}
        })
    },

    async createCard(formData: any) {
        return await instance.post(`announcement`, formData, {
            headers: {Authorization: "Bearer " + localStorage.getItem("token")}    
        })
    }
}












