import React from "react";
import clip from "../../public/paper-clip.svg";
import Image from "next/image";

const Clip = () => {
    return <Image alt="clip" src={clip}></Image>
};

export default Clip;