import React from "react";
import Image from "next/image";
import perfumeNonFillImg from "../../public/DUHI__non_Fill.svg";

const PerfumeNonFill = () => {
    return (
        <div>
            <Image src={perfumeNonFillImg}></Image>
        </div>
    )
}

export default PerfumeNonFill;