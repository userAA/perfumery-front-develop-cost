import React from "react";
import send from "../../public/send.svg";
import Image from "next/image";

const Send = () => {
    return <Image src={send} alt="send"></Image>
};

export default Send;