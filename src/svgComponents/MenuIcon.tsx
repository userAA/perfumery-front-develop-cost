import React from "react";

function MenuIcon() {
  return (
    <svg
      width={22}
      height={6}
      viewBox="0 0 22 6"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M3.024 6c1.67 0 3.024-1.343 3.024-3S4.694 0 3.024 0 0 1.343 0 3s1.354 3 3.024 3zM11 6c1.67 0 3.024-1.343 3.024-3S12.67 0 11 0 7.976 1.343 7.976 3 9.329 6 11 6zM18.976 6C20.646 6 22 4.657 22 3s-1.354-3-3.024-3-3.024 1.343-3.024 3 1.354 3 3.024 3z"
        fill="#27213C"
      />
    </svg>
  );
}

export default MenuIcon;
