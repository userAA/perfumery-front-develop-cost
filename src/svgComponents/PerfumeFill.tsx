import React from "react";
import Image from "next/image";
import perfumeFillImg from "../../public/DUHI_Fill.svg";

const PerfumeFill = () => {
    return (
        <div>
            <Image src={perfumeFillImg} alt="PerfumeFill"></Image>
        </div>
    )
}

export default PerfumeFill;